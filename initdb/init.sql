
SET GLOBAL time_zone='Asia/Seoul';
SET time_zone='Asia/Seoul';
CREATE DATABASE IF NOT EXISTS adminconsole;
CREATE USER IF NOT EXISTS 'acroot'@'%' IDENTIFIED BY 'ac_dev_1234';
GRANT ALL PRIVILEGES ON adminconsole.* TO 'acroot'@'%' WITH GRANT OPTION;

USE adminconsole;

