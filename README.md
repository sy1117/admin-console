# Admin Console

> 관리자 콘솔 레파지터리에 대한 설명을 이곳에 작성하였으며,
> Backend, Frontend 에 관한 상세한 설명은 각 모듈 하위의 README 를 참고바랍니다. 
> - [Backend](server/README.md) 
> - [Frontend](client/README.md) 

## Installation 
1. Node.js 
> [TODO] : Node 버전 호환성 체크
2. VSCode
3. Docker (Optional)
  - 로컬 개발 환경 구동을 위한 DB, Redis 를 띄우는 방법으로 사용함
  - 별도의 설치,실행으로도 가능하다. 
  - DB, Redis 실행을 위한 명령어 
    ```
    docker-compose up -d
    docker-compose stop
    ```


## Scripts 
### Start Server
```
yarn start          // server & client
yarn client         // server port:3000
yarn server         // server port:4000
```

참고 : using Lerna CLI 
```
yarn global add @lerna/cli                    
lerna run --parallel watch
lerna run --scope @admin-console/client start
```

### Lint
```
yarn lint 
```

### Test
```command
yarn test 
```


### Generate 
```command
yarn generate
``` 
GraphQL 스키마를 기반으로 타입 인터페이스를 생성한다. [자세한 설명은 여기에](packages/codegen/README.md)

## ESLint & Code Formatting 

- Git Commit 시, Stage에 올라간 파일들에 대해서 Eslint + Prettier(Code Formatting) 을 수행한다. [자세한 설명은 여기에](packages/eslint/README.md)

### Folder Structure 
```
admin-console
│   README.md
|   package.lock.json         
│   package.json
│   lerna.json                # Lerna Config
│ 
└───.config
│    └─── husky             
│             pre-commit             # Pre-Commit 시, Eslint 
│
└───.vscode                 
│      settings.json          # VSCode Prettier + Eslint
|   
└───client                    # React Application
│   
│   
└───server                    # Nest Application

└───packages
     └─── codegen             # GraphQL 스키마와 쿼리로 부터 타입 인터페이스와 Hook 을 생성해주는 모듈 
     └─── eslint              # Eslint Config    
     └─── prettier            # Prettier Config    
```

