import { Module } from '@nestjs/common';
import { DBConfigModule } from './db.config.module';
import { GraphqlConfigModule } from './graphql.config.module';
import { LogConfigModule } from './log.config.module';
import { SessionConfigModule } from './session.config.module';

@Module({
  imports: [
    LogConfigModule,
    DBConfigModule,
    SessionConfigModule,
    GraphqlConfigModule,
  ],
})
export class CommonConfigModule {}
