import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule } from 'nestjs-pino';
import { v4 as uuidv4 } from 'uuid';

@Module({
  imports: [
    // LoggerModule.forRoot(),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        pinoHttp: {
          level: configService.get('LOG_LEVEL'),
          useLevel: configService.get('LOG_LEVEL'),
          serializers: {
            req(req) {
              req.body = req.raw.body;
              return req;
            },
          },

          autoLogging: false,
          prettyPrint:
            // configService.get('NODE_ENV') !== 'prod'
            {
              colorize: true,
              ignore: 'req,res,hostname,reqId,responseTime,context',
              translateTime: 'SYS:mm/dd/yyyy h:MM:ss TT o',
              messageFormat: `[ RequestId:{pid}-{req.id} ] [ {context} ] {msg} `,
              useLevelLabels: true,
            },
        },
      }),
      inject: [ConfigService],
    }),
  ],
})
export class LogConfigModule {}
