import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { join } from 'path';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/_generated/schema.gql'),
      cors: {
        origin: 'http://localhost:3000',
        credentials: true,
      },
      context: ({ req }) => ({ req }),
      formatError: (error: GraphQLError) => {
        const { code, ...excludeExtensions } = error.extensions;
        const graphQLFormattedError: GraphQLFormattedError = {
          message: error.message,
          locations: undefined,
          path: undefined,
          extensions: { code },
        };
        return graphQLFormattedError;
      },
    }),
  ],
})
export class GraphqlConfigModule {}
