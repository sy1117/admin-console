import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RedisModule, RedisModuleOptions } from 'nestjs-redis';
@Module({
  imports: [
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],

      useFactory: (configService: ConfigService): RedisModuleOptions => ({
        host: configService.get('REDIS_HOST'),
        port: configService.get<number>('REDIS_PORT'),
        password: configService.get('REDIS_PASSWORD'),
        keyPrefix: configService.get('REDIS_PREFIX'),
      }),
    }),
  ],
})
export class RedisConfigModule {}
