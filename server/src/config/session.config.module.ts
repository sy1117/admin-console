import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as ConnectRedis from 'connect-redis';
import * as session from 'express-session';
import { RedisService } from 'nestjs-redis';
import { NestSessionOptions, SessionModule } from 'nestjs-session';
import { RedisConfigModule } from './redis.config.module';

const RedisStore = ConnectRedis(session);

@Module({
  imports: [
    SessionModule.forRootAsync({
      imports: [ConfigModule, RedisConfigModule],
      useFactory: (
        configService: ConfigService,
        redisService: RedisService,
      ): NestSessionOptions => {
        const redisClient = redisService.getClient();
        const store = new RedisStore({ client: redisClient as any });
        return {
          session: {
            store:
              configService.get('REDIS_ENABLE') === 'true'
                ? store
                : new session.MemoryStore(),
            secret: configService.get('SESSION_SECRET'),
            cookie: {
              maxAge: Number(configService.get('SESSION_MAX_AGE')),
            },
          },
        };
      },
      inject: [ConfigService, RedisService],
    }),
  ],
})
export class SessionConfigModule {}
