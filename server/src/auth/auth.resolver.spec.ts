import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { LoggerModule } from 'nestjs-pino';
import { v4 as uuidv4 } from 'uuid';
import { User } from '../users/entities/user.entity';
import { AuthResolver } from './auth.resolver';

describe('AuthResolver', () => {
  let resolver: AuthResolver;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        LoggerModule.forRoot(),
      ],
      providers: [AuthResolver],
    }).compile();

    resolver = module.get<AuthResolver>(AuthResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  describe('테스트 데이터 및 트랜잭션 설정', () => {
    const makeTestUser = async () => {
      const uuid = uuidv4();
      const user = new User();
      user.name = uuid;
      user.email = uuid + '@sk.com';
      user.password = uuid;

      return user;
    };

    it('로그인', async () => {
      const mockUser = await makeTestUser();
      const result = await resolver.login(
        mockUser,
        mockUser.email,
        mockUser.password,
      );

      expect(result).toEqual(mockUser);
    });

    it('로그아웃', async () => {
      const mockUser = await makeTestUser();
      const mockContext = {
        req: {
          user: mockUser,
          session: {
            destroy: async () => {},
          },
        },
      };
      const result = await resolver.logout(mockContext);

      expect(result).toEqual(mockUser);
    });

    it('Who am i', async () => {
      const mockUser = await makeTestUser();

      const result = await resolver.whoAmI(mockUser);

      expect(result).toEqual(mockUser);
    });
  });
});
