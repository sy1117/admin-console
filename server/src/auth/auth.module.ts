import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from '../users/users.module';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.properts';
import { PassportSessionModule } from './passport-session.module';
import { SessionSerializer } from './session.serializer';

@Module({
  imports: [
    UsersModule,
    PassportModule.register({ session: true }),
    PassportSessionModule.register(),
  ],
  providers: [AuthService, LocalStrategy, AuthResolver, SessionSerializer],
})
export class AuthModule {}
