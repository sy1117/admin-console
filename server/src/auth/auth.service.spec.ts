import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import * as bcrypt from 'bcrypt';
import { LoggerModule } from 'nestjs-pino';
import { Connection, QueryRunner } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { InvalidUserError } from '../common/errors/custom.user.error';
import { DBConfigModule } from '../config/db.config.module';
import { User } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let mockUsersService: UsersService;

  let queryRunner: QueryRunner;
  let connection: Connection;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        DBConfigModule,
        LoggerModule.forRoot(),
      ],
      providers: [
        AuthService,
        {
          provide: 'UsersService',
          useValue: {
            findByEmail: jest.fn(),
            signinFail: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    mockUsersService = module.get<UsersService>(UsersService);

    connection = module.get<Connection>(Connection);

    queryRunner = connection.createQueryRunner('master');
    await queryRunner.connect();
  });

  afterAll(async () => {
    await queryRunner.connection.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('테스트 데이터 및 트랜잭션 설정', () => {
    const makeTestUserWithPassword = async (password: string) => {
      const uuid = uuidv4();
      const user = new User();
      user.name = uuid;
      user.email = uuid + '@sk.com';
      user.password = await bcrypt.hash(password, await bcrypt.genSalt());

      return user;
    };

    it('사용자 validation 성공', async () => {
      const password = '1234';
      const mockUser = await makeTestUserWithPassword('1234');

      const findSpy = jest
        .spyOn(mockUsersService, 'findByEmail')
        .mockImplementationOnce(() => Promise.resolve(mockUser));

      const validatedUser = await service.validateUser(
        mockUser.email,
        password,
      );

      expect(validatedUser).toMatchObject({
        name: mockUser.name,
        email: mockUser.email,
      });
      expect(validatedUser.password).toBeUndefined();

      expect(findSpy).toBeCalledTimes(1);
      findSpy.mockRestore();
    });

    it('사용자 validation 실패', async () => {
      const password = '1234';
      const mockUser = await makeTestUserWithPassword('1234');
      const mockAnotherUser = await makeTestUserWithPassword('12345');

      const findSpy = jest
        .spyOn(mockUsersService, 'findByEmail')
        .mockImplementationOnce(() => Promise.resolve(mockAnotherUser));

      const singinFailSpy = jest
        .spyOn(mockUsersService, 'signinFail')
        .mockImplementationOnce(() => Promise.resolve());

      await expect(
        service.validateUser(mockUser.email, password),
      ).rejects.toThrowError(InvalidUserError);

      expect(findSpy).toBeCalledTimes(1);
      expect(singinFailSpy).toBeCalledTimes(1);
      findSpy.mockRestore();
    });
  });
});
