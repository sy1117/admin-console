import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PinoLogger } from 'nestjs-pino';
import { User } from '../users/entities/user.entity';
import { GqlCurrentContext } from './decorators/current-user.decorator';
import { SessionAccount } from './decorators/session-account.decorator';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { LoginGuard } from './guards/login.guard';

@Resolver()
export class AuthResolver {
  constructor(private readonly log: PinoLogger) {
    log.setContext(AuthResolver.name);
  }

  @UseGuards(LoginGuard)
  @Mutation(() => User)
  async login(
    @SessionAccount() user,
    @Args('email') email: string,
    @Args('password') password: string,
  ): Promise<User> {
    this.log.info(`사용자 로그인. email : ${user.email}`);
    return user;
  }

  @UseGuards(AuthenticatedGuard)
  @Mutation(() => User)
  async logout(@GqlCurrentContext() context): Promise<User> {
    const { user } = context.req;
    await context.req.session.destroy();
    this.log.info(`사용자 로그아웃. email : ${user.email}`);
    return user;
  }

  @UseGuards(AuthenticatedGuard)
  @Query(() => User)
  async whoAmI(@SessionAccount() user: User): Promise<User> {
    return user;
  }
  // @UseGuards(AuthenticatedGuard)
  // @Query(() => User)
  // async suser(@Args('email') email: string): Promise<User> {
  //   return this.usersService.findByEmail(email);
  // }

  // @UseGuards(AuthenticatedGuard)
  // @Query(() => Number)
  // async views(@GqlSession() session: { views?: number }): Promise<number> {
  //   console.log(session);
  //   session.views = (session.views || 0) + 1;
  //   return session.views;
  // }
}
