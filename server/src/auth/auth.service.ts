import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { InvalidUserError } from '../common/errors/custom.user.error';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UsersService) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findByEmail(email);

    if (user) {
      if (await bcrypt.compare(password, user.password)) {
        const { password: _password, ...result } = user;
        return result;
      }
      await this.usersService.signinFail(email);
      throw new InvalidUserError();
    }

    return null;
  }
}
