import * as passport from 'passport';

// ********** V2 ************
import {
  DynamicModule,
  MiddlewareConsumer,
  RequestMethod,
} from '@nestjs/common';

const DEFAULT_ROUTES = [{ path: '*', method: RequestMethod.ALL }];

export class PassportSessionModule {
  static register(): DynamicModule {
    return {
      module: PassportSessionModule,
    };
  }

  configure(consumer: MiddlewareConsumer): void {
    const middlewares = [passport.initialize(), passport.session()];
    consumer.apply(...middlewares).forRoutes(...DEFAULT_ROUTES);
  }
}
