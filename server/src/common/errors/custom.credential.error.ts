import { ApolloError } from 'apollo-server-errors';

export class UnauthorizedCredentialClient extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : '접근권한이 없는 Credential ClientId 입니다.',
      'UNAUTHORIZED_CREDENTIAL_CLIENT',
    );

    Object.defineProperty(this, 'name', {
      value: 'UnauthorizedCredentialClientError',
    });
  }
}

export class MaxCredentialError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : 'Credential은 최대 10개까지 만들 수 있습니다.',
      'MAX_CREDENTIAL_ERROR',
    );

    Object.defineProperty(this, 'name', {
      value: 'MaxCredentialError',
    });
  }
}
