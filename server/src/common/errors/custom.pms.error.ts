import { ApolloError } from 'apollo-server-errors';

export class CreatePmsTokenError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : 'PMS Access Token 생성에 실패했습니다.',
      'CREATE_PMS_TOKEN',
    );

    Object.defineProperty(this, 'name', { value: 'CreatePmsTokenError' });
  }
}

export class CreatePmsProviderError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : 'PMS Provider 생성에 실패했습니다.',
      'CREATE_PMS_PROVIDER',
    );

    Object.defineProperty(this, 'name', { value: 'CreatePmsProviderError' });
  }
}

export class CreatePmsClientError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : 'PMS Client 추가 생성에 실패했습니다.',
      'CREATE_PMS_CLIENT',
    );

    Object.defineProperty(this, 'name', { value: 'CreatePmsClientError' });
  }
}

export class DeletePmsProviderError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : 'PMS Provier 삭제에 실패했습니다.',
      'DELETE_PMS_PROVIDER',
    );

    Object.defineProperty(this, 'name', { value: 'DeletePmsProviderError' });
  }
}
export class DeletePmsClientError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : 'PMS Client 삭제에 실패했습니다.',
      'DELETE_PMS_CLIENT',
    );

    Object.defineProperty(this, 'name', { value: 'DeletePmsClientError' });
  }
}
