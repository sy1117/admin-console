import { ObjectType } from '@nestjs/graphql';
import { ApolloError } from 'apollo-server-errors';

export class LockedUserError extends ApolloError {
  constructor(message: string) {
    super(message, 'LOCKED_USER');

    Object.defineProperty(this, 'name', { value: 'LockedUserError' });
  }
}

export class InvalidPasswordError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : '비밀번호가 일치하지 않습니다.',
      'INVALID_PASSWORD',
    );

    Object.defineProperty(this, 'name', { value: 'InvalidPasswordError' });
  }
}

@ObjectType()
export class InvalidUserError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : '가입하지 않은 아이디이거나 잘못된 비밀번호입니다.',
      'INVALID_USER',
    );

    Object.defineProperty(this, 'name', { value: 'InvalidUserError' });
  }
}
