import { ApolloError } from 'apollo-server-errors';

export class ExternalApiError extends ApolloError {
  constructor(message?: string) {
    super(
      message ? message : '외부 다른 서비스 api 호출에 실패했습니다.',
      'EXTERNAL_API',
    );

    Object.defineProperty(this, 'name', { value: 'ExternalApiError' });
  }
}
