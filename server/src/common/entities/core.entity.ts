import { Field, InputType, ObjectType } from '@nestjs/graphql';
import {
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@InputType({ isAbstract: true })
@ObjectType({ isAbstract: true })
export default class CoreEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ type: 'timestamp' })
  @Field(() => Date, {
    nullable: false,
  })
  createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  @Field(() => Date, {
    nullable: false,
  })
  updatedAt!: Date;
}
