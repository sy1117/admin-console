import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
};

export enum Credential_Provider_Status {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export enum Credential_Scope {
  Middleware = 'MIDDLEWARE',
  Krs = 'KRS'
}

export type CreateCredentialInput = {
  /** Name */
  name: Scalars['String'];
  /** Scope: MIDDLEWARE,KRS */
  scopes: Array<Credential_Scope>;
};

export type CreateUserInput = {
  /** Email */
  email: Scalars['String'];
  /** Password */
  password: Scalars['String'];
  /** Password Check */
  passwordCheck: Scalars['String'];
  /** Name */
  name: Scalars['String'];
};

export type CredentialClient = {
  __typename?: 'CredentialClient';
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  credentialName: Scalars['String'];
  clientId: Scalars['String'];
  secret: Scalars['String'];
  scopes: Array<CredentialClientScope>;
};

export type CredentialClientScope = {
  __typename?: 'CredentialClientScope';
  scope: Credential_Scope;
  credentialClient: CredentialClient;
};

export type CredentialProvider = {
  __typename?: 'CredentialProvider';
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  providerName: Scalars['String'];
  providerCode: Scalars['String'];
  status: Credential_Provider_Status;
  credentialClients: Array<CredentialClient>;
  user: User;
};


export type Mutation = {
  __typename?: 'Mutation';
  createUser: User;
  updateUser: User;
  removeUser: User;
  login: User;
  logout: User;
  createCredential: CredentialClient;
  deleteCredential: Scalars['Boolean'];
};


export type MutationCreateUserArgs = {
  createUserInput: CreateUserInput;
};


export type MutationUpdateUserArgs = {
  updateUserInput: UpdateUserInput;
  email: Scalars['String'];
};


export type MutationRemoveUserArgs = {
  email: Scalars['String'];
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  email: Scalars['String'];
};


export type MutationCreateCredentialArgs = {
  createCredentialInput: CreateCredentialInput;
};


export type MutationDeleteCredentialArgs = {
  clientId: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  healthCheck: Scalars['Boolean'];
  users?: Maybe<Array<User>>;
  user?: Maybe<User>;
  whoAmI: User;
  credentials: Array<CredentialClient>;
  credential: CredentialClient;
};


export type QueryUserArgs = {
  email: Scalars['String'];
};


export type QueryCredentialArgs = {
  clientId: Scalars['String'];
};

export type UpdateUserInput = {
  /** Currnet password */
  currentPassword: Scalars['String'];
  /** New password */
  newPassword?: Maybe<Scalars['String']>;
  /** New password check */
  newPasswordCheck?: Maybe<Scalars['String']>;
  /** Name */
  name?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  email: Scalars['String'];
  name: Scalars['String'];
  signinFailCount: Scalars['Float'];
  isLocked: Scalars['Boolean'];
  credentialProvider: CredentialProvider;
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  CREDENTIAL_PROVIDER_STATUS: Credential_Provider_Status;
  CREDENTIAL_SCOPE: Credential_Scope;
  CreateCredentialInput: CreateCredentialInput;
  String: ResolverTypeWrapper<Scalars['String']>;
  CreateUserInput: CreateUserInput;
  CredentialClient: ResolverTypeWrapper<CredentialClient>;
  CredentialClientScope: ResolverTypeWrapper<CredentialClientScope>;
  CredentialProvider: ResolverTypeWrapper<CredentialProvider>;
  DateTime: ResolverTypeWrapper<Scalars['DateTime']>;
  Mutation: ResolverTypeWrapper<{}>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Query: ResolverTypeWrapper<{}>;
  UpdateUserInput: UpdateUserInput;
  User: ResolverTypeWrapper<User>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  CreateCredentialInput: CreateCredentialInput;
  String: Scalars['String'];
  CreateUserInput: CreateUserInput;
  CredentialClient: CredentialClient;
  CredentialClientScope: CredentialClientScope;
  CredentialProvider: CredentialProvider;
  DateTime: Scalars['DateTime'];
  Mutation: {};
  Boolean: Scalars['Boolean'];
  Query: {};
  UpdateUserInput: UpdateUserInput;
  User: User;
  Float: Scalars['Float'];
};

export type CredentialClientResolvers<ContextType = any, ParentType extends ResolversParentTypes['CredentialClient'] = ResolversParentTypes['CredentialClient']> = {
  createdAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  updatedAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  credentialName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  clientId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  secret?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  scopes?: Resolver<Array<ResolversTypes['CredentialClientScope']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CredentialClientScopeResolvers<ContextType = any, ParentType extends ResolversParentTypes['CredentialClientScope'] = ResolversParentTypes['CredentialClientScope']> = {
  scope?: Resolver<ResolversTypes['CREDENTIAL_SCOPE'], ParentType, ContextType>;
  credentialClient?: Resolver<ResolversTypes['CredentialClient'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CredentialProviderResolvers<ContextType = any, ParentType extends ResolversParentTypes['CredentialProvider'] = ResolversParentTypes['CredentialProvider']> = {
  createdAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  updatedAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  providerName?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  providerCode?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  status?: Resolver<ResolversTypes['CREDENTIAL_PROVIDER_STATUS'], ParentType, ContextType>;
  credentialClients?: Resolver<Array<ResolversTypes['CredentialClient']>, ParentType, ContextType>;
  user?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface DateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['DateTime'], any> {
  name: 'DateTime';
}

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  createUser?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationCreateUserArgs, 'createUserInput'>>;
  updateUser?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationUpdateUserArgs, 'updateUserInput' | 'email'>>;
  removeUser?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationRemoveUserArgs, 'email'>>;
  login?: Resolver<ResolversTypes['User'], ParentType, ContextType, RequireFields<MutationLoginArgs, 'password' | 'email'>>;
  logout?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  createCredential?: Resolver<ResolversTypes['CredentialClient'], ParentType, ContextType, RequireFields<MutationCreateCredentialArgs, 'createCredentialInput'>>;
  deleteCredential?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType, RequireFields<MutationDeleteCredentialArgs, 'clientId'>>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  healthCheck?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  users?: Resolver<Maybe<Array<ResolversTypes['User']>>, ParentType, ContextType>;
  user?: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType, RequireFields<QueryUserArgs, 'email'>>;
  whoAmI?: Resolver<ResolversTypes['User'], ParentType, ContextType>;
  credentials?: Resolver<Array<ResolversTypes['CredentialClient']>, ParentType, ContextType>;
  credential?: Resolver<ResolversTypes['CredentialClient'], ParentType, ContextType, RequireFields<QueryCredentialArgs, 'clientId'>>;
};

export type UserResolvers<ContextType = any, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
  createdAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  updatedAt?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  email?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  signinFailCount?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  isLocked?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  credentialProvider?: Resolver<ResolversTypes['CredentialProvider'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type Resolvers<ContextType = any> = {
  CredentialClient?: CredentialClientResolvers<ContextType>;
  CredentialClientScope?: CredentialClientScopeResolvers<ContextType>;
  CredentialProvider?: CredentialProviderResolvers<ContextType>;
  DateTime?: GraphQLScalarType;
  Mutation?: MutationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  User?: UserResolvers<ContextType>;
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
