import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection, EntityManager, QueryRunner } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { DBConfigModule } from '../config/db.config.module';
import { User } from '../users/entities/user.entity';
import { CredentialsClientsRepository } from './credentials.clients.repository';
import { CredentialClient } from './entities/credential.client.entity';
import { CredentialClientScope } from './entities/credential.client.scope.entity';
import { CredentialProvider } from './entities/credential.provider.entity';
import { CREDENTIAL_PROVIDER_STATUS } from './enums/credentials.provier.status';
import { CREDENTIAL_SCOPE } from './enums/credentials.scope';

describe('CredentialsClientsRepository', () => {
  let repository: CredentialsClientsRepository;

  let configService: ConfigService;

  let entityManager: EntityManager;
  let queryRunner: QueryRunner;
  let connection: Connection;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        DBConfigModule,
        TypeOrmModule.forFeature([CredentialsClientsRepository]),
      ],
      providers: [ConfigService],
    }).compile();

    repository = module.get<CredentialsClientsRepository>(
      CredentialsClientsRepository,
    );

    configService = module.get<ConfigService>(ConfigService);

    entityManager = module.get<EntityManager>(EntityManager);
    connection = module.get<Connection>(Connection);

    // @ts-ignore
    entityManager.queryRunner = connection.createQueryRunner('master');
    queryRunner = entityManager.queryRunner;
    await queryRunner.connect();
  });

  afterAll(async () => {
    await queryRunner.connection.close();
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });

  describe('테스트 데이터 및 트랜잭션 설정', () => {
    const makeTestUser = async () => {
      const uuid = uuidv4();
      const user = new User();
      user.name = uuid;
      user.email = uuid + '@sk.com';
      user.password = 'tester';

      return user;
    };

    const makeTestProvider = async (user: User) => {
      const provider = new CredentialProvider();
      provider.providerCode = uuidv4();
      provider.providerName = user.email;
      provider.status = CREDENTIAL_PROVIDER_STATUS.ACTIVE;
      provider.user = user;

      return provider;
    };

    const makeTestCredential = async (
      name: string,
      scopes: CREDENTIAL_SCOPE[],
    ) => {
      const client = new CredentialClient();
      client.credentialName = name;
      client.clientId = configService.get('CREDENTIAL_PREFIX') + '-' + uuidv4();

      for (let i = 0; i < scopes.length; i++) {
        let scope = new CredentialClientScope();
        scope.scope = scopes[i];
        (await client.scopes).push(scope);
      }

      return client;
    };

    describe('CustomRepository에선 TypeORM 함수 호출이 가능하다', () => {
      it('client findOne 테스트 : with many-to-one relation (Provider)', async () => {
        try {
          await queryRunner.startTransaction();

          const testUser = await entityManager.save(await makeTestUser());
          const testProvider = await makeTestProvider(testUser);
          await entityManager.save(testProvider);
          const testCredential = await makeTestCredential(uuidv4(), [
            CREDENTIAL_SCOPE.MIDDLEWARE,
          ]);
          testCredential.credentialProvider = testProvider;
          await entityManager.save(testCredential);

          const client = await repository.findOne({
            clientId: testCredential.clientId,
          });

          expect(client).toMatchObject({
            clientId: client.clientId,
            credentialName: client.credentialName,
          });

          expect(await client.credentialProvider).toMatchObject({
            providerCode: testProvider.providerCode,
            providerName: testProvider.providerName,
          });
        } catch (e) {
          throw e;
        } finally {
          await queryRunner.rollbackTransaction();
        }
      });
    });
  });
});
