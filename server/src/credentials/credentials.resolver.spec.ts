import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../users/entities/user.entity';
import { CredentialsResolver } from './credentials.resolver';
import { CredentialsService } from './credentials.service';
import { CreateCredentialInput } from './dto/create-credential.Input';
import { CredentialClient } from './entities/credential.client.entity';
import { CREDENTIAL_SCOPE } from './enums/credentials.scope';

describe('CredentialsResolver', () => {
  let resolver: CredentialsResolver;
  let mockService: CredentialsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        CredentialsResolver,
        {
          provide: CredentialsService,
          useValue: {
            createCredentialWithProviderIfIsNotExist: jest.fn(),
            findCredentialsAreOwnedByUser: jest.fn(),
            findCredentialIfIsOwnedByUser: jest.fn(),
            deleteCredential: jest.fn(),
          },
        },
      ],
    }).compile();

    resolver = module.get<CredentialsResolver>(CredentialsResolver);
    mockService = module.get<CredentialsService>(CredentialsService);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  describe('테스트 더미 객체 생성', () => {
    const testUser = new User();
    const testClientId = 'credentialsResolver-id';

    it('Credential 생성', async () => {
      const createCredentialInput: CreateCredentialInput = new CreateCredentialInput();

      createCredentialInput.name = 'credentialsProviderRepo-name';
      createCredentialInput.scopes = [
        CREDENTIAL_SCOPE.KRS,
        CREDENTIAL_SCOPE.MIDDLEWARE,
      ];

      const mockResult = new CredentialClient();

      const spy = jest
        .spyOn(mockService, 'createCredentialWithProviderIfIsNotExist')
        .mockImplementationOnce(() => Promise.resolve(mockResult));

      const result = await resolver.createCredential(
        testUser,
        createCredentialInput,
      );

      expect(
        mockService.createCredentialWithProviderIfIsNotExist,
      ).toHaveBeenCalledTimes(1);
      expect(result).toEqual(mockResult);
    });

    it('Credential 목록 조회', async () => {
      const mockResult = [new CredentialClient(), new CredentialClient()];

      const spy = jest
        .spyOn(mockService, 'findCredentialsAreOwnedByUser')
        .mockImplementationOnce(() => Promise.resolve(mockResult));

      const result = await resolver.credentials(testUser);

      expect(mockService.findCredentialsAreOwnedByUser).toHaveBeenCalledTimes(
        1,
      );
      expect(result).toEqual(mockResult);
    });

    it('Credential 단건 조회', async () => {
      const mockResult = new CredentialClient();

      const spy = jest
        .spyOn(mockService, 'findCredentialIfIsOwnedByUser')
        .mockImplementationOnce(() => Promise.resolve(mockResult));

      const result = await resolver.credential(testUser, testClientId);

      expect(mockService.findCredentialIfIsOwnedByUser).toHaveBeenCalledTimes(
        1,
      );
      expect(result).toEqual(mockResult);
    });

    it('Credential 삭제', async () => {
      const mockResult = true;

      const spy = jest
        .spyOn(mockService, 'deleteCredential')
        .mockImplementationOnce(() => Promise.resolve(mockResult));

      const result = await resolver.deleteCredential(testUser, testClientId);

      expect(mockService.deleteCredential).toHaveBeenCalledTimes(1);
      expect(result).toEqual(mockResult);
    });
  });
});
