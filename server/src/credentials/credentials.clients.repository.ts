import { EntityRepository, Repository } from 'typeorm';
import { CredentialClient } from './entities/credential.client.entity';

@EntityRepository(CredentialClient)
export class CredentialsClientsRepository extends Repository<CredentialClient> {}
