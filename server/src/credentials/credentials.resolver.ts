import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { SessionAccount } from '../auth/decorators/session-account.decorator';
import { AuthenticatedGuard } from '../auth/guards/authenticated.guard';
import { User } from '../users/entities/user.entity';
import { CredentialsService } from './credentials.service';
import { CreateCredentialInput } from './dto/create-credential.Input';
import { CredentialClient } from './entities/credential.client.entity';

@Resolver(() => CredentialClient)
export class CredentialsResolver {
  constructor(private readonly credentialsService: CredentialsService) {}

  @UseGuards(AuthenticatedGuard)
  @Mutation(() => CredentialClient)
  async createCredential(
    @SessionAccount() user: User,
    @Args('createCredentialInput')
    createCredentialInput: CreateCredentialInput,
  ): Promise<CredentialClient> {
    return this.credentialsService.createCredentialWithProviderIfIsNotExist(
      user,
      createCredentialInput,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Query(() => [CredentialClient])
  async credentials(@SessionAccount() user: User): Promise<CredentialClient[]> {
    return this.credentialsService.findCredentialsAreOwnedByUser(user);
  }

  @UseGuards(AuthenticatedGuard)
  @Query(() => CredentialClient)
  async credential(
    @SessionAccount() user: User,
    @Args('clientId')
    clientId: string,
  ): Promise<CredentialClient> {
    return await this.credentialsService.findCredentialIfIsOwnedByUser(
      user,
      clientId,
    );
  }

  @UseGuards(AuthenticatedGuard)
  @Mutation(() => Boolean)
  async deleteCredential(
    @SessionAccount() user: User,
    @Args('clientId')
    clientId: string,
  ): Promise<Boolean> {
    return await this.credentialsService.deleteCredential(user, clientId);
  }
}
