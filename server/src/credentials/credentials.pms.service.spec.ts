import { HttpService } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { LoggerModule } from 'nestjs-pino';
import { of } from 'rxjs';
import {
  CreatePmsClientError,
  CreatePmsProviderError,
  CreatePmsTokenError,
  DeletePmsClientError,
  DeletePmsProviderError,
} from '../common/errors/custom.pms.error';
import { CredentialsPmsService } from './credentials.pms.service';
import { CREDENTIAL_SCOPE } from './enums/credentials.scope';

describe('CredentialsPmsService', () => {
  let service: CredentialsPmsService;
  let httpServiceMock: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        LoggerModule.forRoot(),
      ],
      providers: [
        CredentialsPmsService,
        {
          provide: 'HttpService',
          useValue: {
            post: jest.fn(),
            delete: jest.fn(),
            toPromise: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<CredentialsPmsService>(CredentialsPmsService);
    httpServiceMock = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('PMS Provider/client 생성 테스트', () => {
    const testMethodParams = {
      providerName: 'pmsService',
      providerCode: 'pmsService',
      clientId: 'pmsService-id',
      scopes: [CREDENTIAL_SCOPE.KRS, CREDENTIAL_SCOPE.MIDDLEWARE],
    };

    const accessTokenResponse = {
      data: {
        access_token: 'TEST_ADMIN_ACCESS_TOKEN',
      },
      headers: {},
      config: { url: 'pmsService_keycloakurl' },
      status: 200,
      statusText: 'OK',
    };

    beforeEach(async () => {});

    it('PMS 에 provider 생성을 요청한다.', async () => {
      const mockPostProviderResponse = {
        data: {
          result: {},
        },
        headers: {},
        config: { url: 'pms_post_provider' },
        status: 200,
        statusText: 'OK',
      };

      jest
        .spyOn(httpServiceMock, 'post')
        .mockImplementationOnce(() => of(accessTokenResponse));

      jest
        .spyOn(httpServiceMock, 'post')
        .mockImplementationOnce(() => of(mockPostProviderResponse));

      const res = await service.createPmsProvider(
        testMethodParams.providerName,
        testMethodParams.clientId,
        testMethodParams.scopes,
      );

      expect(res).toMatchObject(mockPostProviderResponse.data.result);
    });

    describe('PMS 에서 provider 생성요청시 발생가능한 에러', () => {
      it('AccessToken 요청에 실패', async () => {
        jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.createPmsProvider(
            testMethodParams.providerName,
            testMethodParams.clientId,
            testMethodParams.scopes,
          ),
        ).rejects.toThrow(CreatePmsTokenError);
      });

      it('post 요청 Unexpected error', async () => {
        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.createPmsProvider(
            testMethodParams.providerName,
            testMethodParams.clientId,
            testMethodParams.scopes,
          ),
        ).rejects.toThrow(CreatePmsProviderError);
      });

      it('post 200 ok가 아닌 경우', async () => {
        const mockPostResponse = {
          data: {
            result: {},
          },
          headers: {},
          config: { url: 'pms_api' },
          status: 400,
          statusText: 'BAD_REQUEST',
        };

        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(mockPostResponse));

        await expect(
          service.createPmsProvider(
            testMethodParams.providerName,
            testMethodParams.clientId,
            testMethodParams.scopes,
          ),
        ).rejects.toThrow(CreatePmsProviderError);
      });
    });

    it('PMS 에 client 생성을 요청한다.', async () => {
      const mockPostClientResponse = {
        data: {
          result: {},
        },
        headers: {},
        config: { url: 'pms_post_client' },
        status: 200,
        statusText: 'OK',
      };

      jest
        .spyOn(httpServiceMock, 'post')
        .mockImplementationOnce(() => of(accessTokenResponse));

      jest
        .spyOn(httpServiceMock, 'post')
        .mockImplementationOnce(() => of(mockPostClientResponse));

      const res = await service.createPmsClient(
        testMethodParams.providerCode,
        testMethodParams.clientId,
        testMethodParams.scopes,
      );

      expect(res).toMatchObject(mockPostClientResponse.data.result);
    });

    describe('PMS 에서 client 생성요청시 발생가능한 에러', () => {
      it('AccessToken 요청에 실패', async () => {
        jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.createPmsClient(
            testMethodParams.providerName,
            testMethodParams.clientId,
            testMethodParams.scopes,
          ),
        ).rejects.toThrow(CreatePmsTokenError);
      });

      it('post 요청 Unexpected error', async () => {
        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.createPmsClient(
            testMethodParams.providerCode,
            testMethodParams.clientId,
            testMethodParams.scopes,
          ),
        ).rejects.toThrow(CreatePmsClientError);
      });

      it('post 200 ok가 아닌 경우', async () => {
        const mockPostResponse = {
          data: {
            result: {},
          },
          headers: {},
          config: { url: 'pms_api' },
          status: 400,
          statusText: 'BAD_REQUEST',
        };

        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(mockPostResponse));

        await expect(
          service.createPmsClient(
            testMethodParams.providerName,
            testMethodParams.clientId,
            testMethodParams.scopes,
          ),
        ).rejects.toThrow(CreatePmsClientError);
      });
    });

    it('PMS 에 provider 삭제를 요청한다.', async () => {
      const mockDeleteProviderResponse = {
        data: {
          result: {},
        },
        headers: {},
        config: { url: 'pms_delete_provider' },
        status: 200,
        statusText: 'OK',
      };

      jest
        .spyOn(httpServiceMock, 'post')
        .mockImplementationOnce(() => of(accessTokenResponse));

      jest
        .spyOn(httpServiceMock, 'delete')
        .mockImplementationOnce(() => of(mockDeleteProviderResponse));

      await expect(
        service.deletePmsProvider(testMethodParams.providerCode),
      ).resolves.not.toThrow();
    });

    describe('PMS 에서 provider 삭제요청시 발생가능한 에러', () => {
      it('AccessToken 요청에 실패', async () => {
        jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.deletePmsProvider(testMethodParams.providerCode),
        ).rejects.toThrow(CreatePmsTokenError);
      });

      it('delete 요청 Unexpected error', async () => {
        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest.spyOn(httpServiceMock, 'delete').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.deletePmsProvider(testMethodParams.providerCode),
        ).rejects.toThrow(DeletePmsProviderError);
      });

      it('delete 200 ok가 아닌 경우', async () => {
        const mockDeleteResponse = {
          data: {
            result: {},
          },
          headers: {},
          config: { url: 'pms_api' },
          status: 400,
          statusText: 'BAD_REQUEST',
        };

        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest
          .spyOn(httpServiceMock, 'delete')
          .mockImplementationOnce(() => of(mockDeleteResponse));

        await expect(
          service.deletePmsProvider(testMethodParams.providerName),
        ).rejects.toThrow(DeletePmsProviderError);
      });
    });

    it('PMS 에 client 삭제를 요청한다.', async () => {
      const mockDeleteProviderResponse = {
        data: {
          result: {},
        },
        headers: {},
        config: { url: 'pms_delete_client' },
        status: 200,
        statusText: 'OK',
      };

      jest
        .spyOn(httpServiceMock, 'post')
        .mockImplementationOnce(() => of(accessTokenResponse));

      jest
        .spyOn(httpServiceMock, 'delete')
        .mockImplementationOnce(() => of(mockDeleteProviderResponse));

      await expect(
        service.deletePmsClient(
          testMethodParams.providerCode,
          testMethodParams.clientId,
        ),
      ).resolves.not.toThrow();
    });

    describe('PMS 에서 client 삭제요청시 발생가능한 에러', () => {
      it('AccessToken 요청에 실패', async () => {
        jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.deletePmsClient(
            testMethodParams.providerCode,
            testMethodParams.clientId,
          ),
        ).rejects.toThrow(CreatePmsTokenError);
      });

      it('delete 요청 Unexpected error', async () => {
        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest.spyOn(httpServiceMock, 'delete').mockImplementationOnce(() => {
          throw new Error('unexpected error');
        });

        await expect(
          service.deletePmsClient(
            testMethodParams.providerCode,
            testMethodParams.clientId,
          ),
        ).rejects.toThrow(DeletePmsClientError);
      });

      it('delete 200 ok가 아닌 경우', async () => {
        const mockDeleteResponse = {
          data: {
            result: {},
          },
          headers: {},
          config: { url: 'pms_api' },
          status: 400,
          statusText: 'BAD_REQUEST',
        };

        jest
          .spyOn(httpServiceMock, 'post')
          .mockImplementationOnce(() => of(accessTokenResponse));

        jest
          .spyOn(httpServiceMock, 'delete')
          .mockImplementationOnce(() => of(mockDeleteResponse));

        await expect(
          service.deletePmsClient(
            testMethodParams.providerName,
            testMethodParams.clientId,
          ),
        ).rejects.toThrow(DeletePmsClientError);
      });
    });
  });
});
