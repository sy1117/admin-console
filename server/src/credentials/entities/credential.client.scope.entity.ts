import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CREDENTIAL_SCOPE } from '../enums/credentials.scope';
import { CredentialClient } from './credential.client.entity';

@Entity()
@ObjectType()
export class CredentialClientScope {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: CREDENTIAL_SCOPE,
    nullable: false,
  })
  @Field(() => CREDENTIAL_SCOPE)
  scope?: CREDENTIAL_SCOPE;

  @ManyToOne(() => CredentialClient, (credential) => credential.scopes, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @Field(() => CredentialClient)
  @JoinColumn()
  credentialClient!: CredentialClient;
}
