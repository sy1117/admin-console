import { Field, ObjectType } from '@nestjs/graphql';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  Unique,
} from 'typeorm';
import CoreEntity from '../../common/entities/core.entity';
import { CredentialClientScope } from './credential.client.scope.entity';
import { CredentialProvider } from './credential.provider.entity';

@Entity()
@ObjectType()
@Unique(['credentialName', 'credentialProvider'])
export class CredentialClient extends CoreEntity {
  @Column({ nullable: false })
  @Field()
  credentialName!: string;

  @Column({ nullable: true })
  @Field()
  clientId?: string;

  @Column({ nullable: true, update: false, insert: false })
  @Field()
  secret?: string;

  @OneToMany(
    () => CredentialClientScope,
    (credentialScope) => credentialScope.credentialClient,
    { nullable: false, cascade: true, eager: true },
  )
  @Field(() => [CredentialClientScope])
  scopes?: Promise<CredentialClientScope[]>;

  @ManyToOne(
    () => CredentialProvider,
    (credentialProvider) => credentialProvider.credentialClients,
    {
      nullable: false,
      lazy: true,
      onDelete: 'CASCADE',
    },
  )
  @JoinColumn()
  credentialProvider!: CredentialProvider;
}
