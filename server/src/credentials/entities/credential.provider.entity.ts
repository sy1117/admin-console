import { Field, ObjectType } from '@nestjs/graphql';
import CoreEntity from '../../common/entities/core.entity';
import { User } from '../../users/entities/user.entity';
import { Column, Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { CredentialClient } from './credential.client.entity';
import { CREDENTIAL_PROVIDER_STATUS } from '../enums/credentials.provier.status';

@Entity()
@ObjectType()
export class CredentialProvider extends CoreEntity {
  @Column({ nullable: true })
  @Field()
  providerName?: string;

  @Column({ nullable: true })
  @Field()
  providerCode?: string;

  @Column({
    type: 'enum',
    enum: CREDENTIAL_PROVIDER_STATUS,
    nullable: false,
  })
  @Field(() => CREDENTIAL_PROVIDER_STATUS)
  status!: CREDENTIAL_PROVIDER_STATUS;

  @OneToMany(
    () => CredentialClient,
    (credentialClient) => credentialClient.credentialProvider,
    { nullable: false, cascade: true, lazy: true },
  )
  @Field(() => [CredentialClient])
  credentialClients?: CredentialClient[];

  @OneToOne(() => User, (user) => user.credentialProvider, {
    nullable: false,
    cascade: false,
    lazy: true,
  })
  @JoinColumn()
  @Field(() => User)
  user!: User;
}
