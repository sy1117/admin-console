import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserInputError } from 'apollo-server-errors';
import { LoggerModule } from 'nestjs-pino';
import { Connection, QueryRunner } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import {
  MaxCredentialError,
  UnauthorizedCredentialClient,
} from '../common/errors/custom.credential.error';
import { DBConfigModule } from '../config/db.config.module';
import { User } from '../users/entities/user.entity';
import { CredentialsClientsRepository } from './credentials.clients.repository';
import { CredentialsPmsService } from './credentials.pms.service';
import { CredentialsProvidersRepository } from './credentials.providers.repository';
import { CredentialsService } from './credentials.service';
import { CreateCredentialInput } from './dto/create-credential.Input';
import { CredentialClient } from './entities/credential.client.entity';
import { CredentialClientScope } from './entities/credential.client.scope.entity';
import { CredentialProvider } from './entities/credential.provider.entity';
import { CREDENTIAL_PROVIDER_STATUS } from './enums/credentials.provier.status';
import { CREDENTIAL_SCOPE } from './enums/credentials.scope';

describe('CredentialsService', () => {
  let service: CredentialsService;
  let providerRepository: CredentialsProvidersRepository;
  let clientRepository: CredentialsClientsRepository;

  let pmsService: CredentialsPmsService;
  let configService: ConfigService;

  let queryRunner: QueryRunner;
  let connection: Connection;
  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        DBConfigModule,
        TypeOrmModule.forFeature([
          CredentialsProvidersRepository,
          CredentialsClientsRepository,
        ]),
        LoggerModule.forRoot(),
      ],
      providers: [
        {
          provide: 'CredentialsPmsService',
          useValue: {
            createPmsProvider: jest.fn(),
            createPmsClient: jest.fn(),
            deletePmsClient: jest.fn(),
            deletePmsProvider: jest.fn(),
          },
        },
        CredentialsService,
        ConfigService,
      ],
    }).compile();

    service = module.get<CredentialsService>(CredentialsService);
    providerRepository = module.get<CredentialsProvidersRepository>(
      CredentialsProvidersRepository,
    );
    clientRepository = module.get<CredentialsClientsRepository>(
      CredentialsClientsRepository,
    );
    pmsService = module.get<CredentialsPmsService>(CredentialsPmsService);
    configService = module.get<ConfigService>(ConfigService);

    connection = module.get<Connection>(Connection);

    queryRunner = connection.createQueryRunner('master');
    await queryRunner.connect();
  });

  afterAll(async () => {
    await queryRunner.connection.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  const makeTestUser = async () => {
    const uuid = uuidv4();
    const user = new User();
    user.name = uuid;
    user.email = uuid + '@sk.com';
    user.password = uuid;

    return user;
  };

  const makeTestProvider = async (user: User, providerCode: string) => {
    const provider = new CredentialProvider();
    provider.providerCode = providerCode;
    provider.providerName = user.email;
    provider.status = CREDENTIAL_PROVIDER_STATUS.INACTIVE;
    provider.user = user;

    return provider;
  };
  const makeTestCredential = async (
    name: string,
    scopes: CREDENTIAL_SCOPE[],
  ) => {
    const client = new CredentialClient();
    client.credentialName = name;
    client.clientId = configService.get('CREDENTIAL_PREFIX') + '-' + uuidv4();

    for (let i = 0; i < scopes.length; i++) {
      let scope = new CredentialClientScope();
      scope.scope = scopes[i];
      (await client.scopes).push(scope);
    }

    return client;
  };

  describe('Credential 생성', () => {
    const makeTestCreateCredentialInput = () => {
      const createCredentialInput = new CreateCredentialInput();
      createCredentialInput.name = uuidv4();
      createCredentialInput.scopes = [
        CREDENTIAL_SCOPE.MIDDLEWARE,
        CREDENTIAL_SCOPE.KRS,
      ];

      return createCredentialInput;
    };

    it('1. Creential 을 생성한 적이 없는 사용자가 1개를 생성한다.', async () => {
      const createCredentialInput = makeTestCreateCredentialInput();

      const mockUser = await makeTestUser();
      const mockProvider = await makeTestProvider(mockUser, null);

      (await mockProvider.credentialClients).push(
        await makeTestCredential(
          createCredentialInput.name,
          createCredentialInput.scopes,
        ),
      );

      jest
        .spyOn(providerRepository, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(undefined));

      jest
        .spyOn(providerRepository, 'save')
        .mockImplementationOnce(() => Promise.resolve(mockProvider));

      const mockProviderCode = uuidv4();
      mockProvider.providerCode = mockProviderCode;
      mockProvider.status = CREDENTIAL_PROVIDER_STATUS.ACTIVE;

      jest
        .spyOn(providerRepository, 'save')
        .mockImplementationOnce(() => Promise.resolve(mockProvider));

      const pmsSpy = jest
        .spyOn(pmsService, 'createPmsProvider')
        .mockImplementation(() =>
          Promise.resolve({
            providerCode: mockProviderCode,
            status: 'ACTIVE',
            clients: [
              {
                secret: 'testSecret',
              },
            ],
          }),
        );

      const credential = await service.createCredentialWithProviderIfIsNotExist(
        mockUser,
        createCredentialInput,
      );

      expect(pmsService.createPmsProvider).toHaveBeenCalledTimes(1);

      expect(credential.credentialName).toEqual(createCredentialInput.name);
      expect(credential.clientId).toContain(
        configService.get('CREDENTIAL_PREFIX') + '-',
      );
      expect(credential.id).not.toBeNull();
      expect(credential.secret).not.toBeNull();
      expect(credential.createdAt).not.toBeNull();
      expect(credential.updatedAt).not.toBeNull();

      pmsSpy.mockRestore();
    });

    it('2. Creential 을 생성한 적이 있는 사용자가 1개를 더 추가 생성한다.', async () => {
      const createCredentialInput = makeTestCreateCredentialInput();

      const mockProviderCode = 'P1010101';
      const mockUser = await makeTestUser();
      const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

      (await mockProvider.credentialClients).push(
        await makeTestCredential('exist-credential', [
          CREDENTIAL_SCOPE.MIDDLEWARE,
        ]),
      );
      (await mockProvider.credentialClients).push(
        await makeTestCredential(
          createCredentialInput.name,
          createCredentialInput.scopes,
        ),
      );

      mockProvider.providerCode = mockProviderCode;
      mockProvider.status = CREDENTIAL_PROVIDER_STATUS.ACTIVE;

      jest
        .spyOn(providerRepository, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(mockProvider));

      const saveProviderSpy = jest
        .spyOn(providerRepository, 'save')
        .mockImplementation(() => Promise.resolve(mockProvider));

      const pmsSpy = jest
        .spyOn(pmsService, 'createPmsClient')
        .mockImplementation(() =>
          Promise.resolve({
            providerCode: mockProviderCode,
            status: 'ACTIVE',
            secret: 'testSecret',
          }),
        );

      const credential = await service.createCredentialWithProviderIfIsNotExist(
        mockUser,
        createCredentialInput,
      );

      expect(pmsService.createPmsClient).toBeCalledTimes(1);

      expect(credential.credentialName).toEqual(createCredentialInput.name);
      expect(credential.clientId).toContain(
        configService.get('CREDENTIAL_PREFIX') + '-',
      );
      expect(credential.id).not.toBeNull();
      expect(credential.secret).not.toBeNull();
      expect(credential.createdAt).not.toBeNull();
      expect(credential.updatedAt).not.toBeNull();

      saveProviderSpy.mockRestore();
      pmsSpy.mockRestore();
    });

    it('3. Credential은 10개까지 추가 할 수 있다. throw Error', async () => {
      const createCredentialInput = makeTestCreateCredentialInput();

      const mockProviderCode = 'P1010101';
      const mockUser = await makeTestUser();
      const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

      for (let i = 0; i < 10; i++) {
        (await mockProvider.credentialClients).push(
          await makeTestCredential('exist-credential' + i, [
            CREDENTIAL_SCOPE.MIDDLEWARE,
          ]),
        );
      }

      jest
        .spyOn(providerRepository, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(mockProvider));

      await expect(
        service.createCredentialWithProviderIfIsNotExist(
          mockUser,
          createCredentialInput,
        ),
      ).rejects.toThrow(MaxCredentialError);
    });
  });

  describe('Credential 조회', () => {
    describe('목록 조회', () => {
      it('1. Credential 을 2개 가진 사용자는 2개가 조회된다.', async () => {
        const mockProviderCode = 'P1010101';
        const mockUser = await makeTestUser();
        const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

        for (let i = 0; i < 2; i++) {
          (await mockProvider.credentialClients).push(
            await makeTestCredential('exist-credential' + i, [
              CREDENTIAL_SCOPE.MIDDLEWARE,
            ]),
          );
        }

        jest
          .spyOn(providerRepository, 'findOne')
          .mockImplementationOnce(() => Promise.resolve(mockProvider));

        const credentials = await service.findCredentialsAreOwnedByUser(
          mockUser,
        );

        expect(credentials.length).toEqual(2);
        expect(credentials[0]).toMatchObject({
          credentialName: (await mockProvider.credentialClients)[0]
            .credentialName,
          clientId: (await mockProvider.credentialClients)[0].clientId,
        });
        expect(credentials[1]).toMatchObject({
          credentialName: (await mockProvider.credentialClients)[1]
            .credentialName,
          clientId: (await mockProvider.credentialClients)[1].clientId,
        });

        for (let i = 0; i < 2; i++) {
          expect(credentials[i].id).not.toBeNull();
          expect(credentials[i].createdAt).not.toBeNull();
          expect(credentials[i].updatedAt).not.toBeNull();
          expect(credentials[i].secret).toBeUndefined();
        }
      });

      it('2. Credential을 생성하지 않은 사용자는 0개가 조회된다.', async () => {
        const mockUser = await makeTestUser();
        const credentials = await service.findCredentialsAreOwnedByUser(
          mockUser,
        );

        expect(credentials.length).toEqual(0);
      });
    });

    describe('단건 조회', () => {
      it('1. Credential 을 2개 가진 유저의 첫번째 Credential을 조회한다. : ', async () => {
        const mockProviderCode = 'P1010101';
        const mockUser = await makeTestUser();
        const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

        for (let i = 0; i < 2; i++) {
          let client = await makeTestCredential('exist-credential' + i, [
            CREDENTIAL_SCOPE.MIDDLEWARE,
          ]);
          client.credentialProvider = mockProvider;
          (await mockProvider.credentialClients).push(client);
        }

        const mockFirstCredential = (await mockProvider.credentialClients)[0];

        const findOneSpy = jest
          .spyOn(clientRepository, 'findOne')
          .mockImplementation(() => Promise.resolve(mockFirstCredential));

        const credential = await service.findCredentialIfIsOwnedByUser(
          mockUser,
          mockFirstCredential.clientId,
        );

        expect(credential).toMatchObject({
          credentialName: mockFirstCredential.credentialName,
          clientId: mockFirstCredential.clientId,
        });

        expect(credential.id).not.toBeNull();
        expect(credential.createdAt).not.toBeNull();
        expect(credential.updatedAt).not.toBeNull();
        expect(credential.secret).toBeUndefined();

        findOneSpy.mockRestore();
      });

      it('2. 잘못된 id의 Credential 을 조회시 throw UserInputError 한다.', async () => {
        const mockUser = await makeTestUser();

        await expect(
          service.findCredentialIfIsOwnedByUser(mockUser, 'notExistClientId'),
        ).rejects.toThrow(UserInputError);
      });

      it('3. 다른 사용자의 Credential 을 조회시 throw UnauthorizedCredentialClient 한다.', async () => {
        const mockProviderCode = 'P1010101';
        const mockUser = await makeTestUser();
        const mockAnotherUser = await makeTestUser();
        const mockProvider = await makeTestProvider(
          mockAnotherUser,
          mockProviderCode,
        );

        const mockAnotherUserCredential = await makeTestCredential(
          'exist-credential',
          [CREDENTIAL_SCOPE.MIDDLEWARE],
        );

        mockAnotherUserCredential.credentialProvider = mockProvider;

        const clientFindOneSpy = jest
          .spyOn(clientRepository, 'findOne')
          .mockImplementation(() => Promise.resolve(mockAnotherUserCredential));

        await expect(
          service.findCredentialIfIsOwnedByUser(
            mockUser,
            mockAnotherUserCredential.clientId,
          ),
        ).rejects.toThrow(UnauthorizedCredentialClient);

        clientFindOneSpy.mockRestore();
      });
    });
  });

  describe('Credential 삭제', () => {
    it('1. Credential을 2개 가진 사용자의 첫번쨰 Credential 을 삭제한다.', async () => {
      const mockProviderCode = 'P1010101';
      const mockUser = await makeTestUser();
      const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

      for (let i = 0; i < 2; i++) {
        let client = await makeTestCredential('exist-credential' + i, [
          CREDENTIAL_SCOPE.MIDDLEWARE,
        ]);
        client.credentialProvider = mockProvider;
        (await mockProvider.credentialClients).push(client);
      }

      const mockFirstCredential = (await mockProvider.credentialClients)[0];

      jest
        .spyOn(service, 'findCredentialIfIsOwnedByUser')
        .mockImplementationOnce(() => Promise.resolve(mockFirstCredential));

      jest.spyOn(clientRepository, 'delete').mockImplementation(() => null);

      const pmsSpy = jest
        .spyOn(pmsService, 'deletePmsClient')
        .mockImplementation(() => null);

      const isSuccess = await service.deleteCredential(
        mockUser,
        mockFirstCredential.clientId,
      );

      expect(pmsService.deletePmsClient).toHaveBeenCalledTimes(1);
      expect(isSuccess).toEqual(true);

      pmsSpy.mockRestore();
    });

    it('2. Credential을 1개 가진 사용자의 Credential 을 삭제한다.', async () => {
      const mockProviderCode = 'P1010101';
      const mockUser = await makeTestUser();
      const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

      const mockCredential = await makeTestCredential('exist-credential', [
        CREDENTIAL_SCOPE.MIDDLEWARE,
      ]);
      mockCredential.credentialProvider = mockProvider;

      jest
        .spyOn(service, 'findCredentialIfIsOwnedByUser')
        .mockImplementationOnce(() => Promise.resolve(mockCredential));

      jest.spyOn(providerRepository, 'delete').mockImplementation(() => null);

      const pmsSpy = jest
        .spyOn(pmsService, 'deletePmsProvider')
        .mockImplementation(() => null);

      const isSuccess = await service.deleteCredential(
        mockUser,
        mockCredential.clientId,
      );

      expect(pmsService.deletePmsProvider).toHaveBeenCalledTimes(1);

      expect(isSuccess).toEqual(true);

      pmsSpy.mockRestore();
    });

    it('3. 잘못된 id의 Credential 을 삭제시 throw UserInputError 한다.', async () => {
      const mockProviderCode = 'P1010101';
      const mockUser = await makeTestUser();
      const mockProvider = await makeTestProvider(mockUser, mockProviderCode);

      const mockCredential = await makeTestCredential('exist-credential', [
        CREDENTIAL_SCOPE.MIDDLEWARE,
      ]);
      mockCredential.credentialProvider = mockProvider;

      jest
        .spyOn(clientRepository, 'findOne')
        .mockImplementationOnce(() => Promise.resolve(undefined));

      await expect(
        service.deleteCredential(mockUser, 'notExistClientId'),
      ).rejects.toThrow(UserInputError);
    });

    it('4. 다른 사용자의 Credential 을 삭제시 throw UnauthorizedCredentialClient 한다.', async () => {
      const mockUser = await makeTestUser();

      const mockProviderCode = 'P1010101';
      const mockAnotherUser = await makeTestUser();
      const mockProvider = await makeTestProvider(
        mockAnotherUser,
        mockProviderCode,
      );

      const mockAnotherUserCredential = await makeTestCredential(
        'exist-credential',
        [CREDENTIAL_SCOPE.MIDDLEWARE],
      );

      mockAnotherUserCredential.credentialProvider = mockProvider;

      const clientFindOneSpy = jest
        .spyOn(clientRepository, 'findOne')
        .mockImplementation(() => Promise.resolve(mockAnotherUserCredential));
      await expect(
        service.deleteCredential(mockUser, mockAnotherUserCredential.clientId),
      ).rejects.toThrow(UnauthorizedCredentialClient);

      clientFindOneSpy.mockRestore();
    });
  });
});
