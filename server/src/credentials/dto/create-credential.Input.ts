import { Field, InputType } from '@nestjs/graphql';
import { MaxLength } from 'class-validator';
import { CREDENTIAL_SCOPE } from '../enums/credentials.scope';

@InputType()
export class CreateCredentialInput {
  @MaxLength(30)
  @Field(() => String, { description: 'Name', nullable: false })
  name!: string;

  @Field(() => [CREDENTIAL_SCOPE], {
    description: 'Scope: MIDDLEWARE,KRS',
    nullable: false,
  })
  scopes!: CREDENTIAL_SCOPE[];
}
