import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection, EntityManager, QueryRunner } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { DBConfigModule } from '../config/db.config.module';
import { User } from '../users/entities/user.entity';
import { CredentialsProvidersRepository } from './credentials.providers.repository';
import { CredentialProvider } from './entities/credential.provider.entity';
import { CREDENTIAL_PROVIDER_STATUS } from './enums/credentials.provier.status';

describe('CredentialsProvidersRepository', () => {
  let repository: CredentialsProvidersRepository;

  let entityManager: EntityManager;
  let queryRunner: QueryRunner;
  let connection: Connection;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.test',
          isGlobal: true,
        }),
        DBConfigModule,
        TypeOrmModule.forFeature([CredentialsProvidersRepository]),
      ],
    }).compile();

    repository = module.get<CredentialsProvidersRepository>(
      CredentialsProvidersRepository,
    );

    entityManager = module.get<EntityManager>(EntityManager);
    connection = module.get<Connection>(Connection);

    // @ts-ignore
    entityManager.queryRunner = connection.createQueryRunner('master');
    queryRunner = entityManager.queryRunner;
    await queryRunner.connect();
  });

  afterAll(async () => {
    await queryRunner.connection.close();
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });

  describe('테스트 데이터 및 트랜잭션 설정', () => {
    const makeTestUser = async () => {
      const uuid = uuidv4();
      const user = new User();
      user.name = uuid;
      user.email = uuid + '@sk.com';
      user.password = 'tester';

      return user;
    };

    const makeTestProvider = async (user: User) => {
      const provider = new CredentialProvider();
      provider.providerCode = uuidv4();
      provider.providerName = user.email;
      provider.status = CREDENTIAL_PROVIDER_STATUS.ACTIVE;
      provider.user = user;

      return provider;
    };

    describe('CustomRepository에선 TypeORM 함수 호출이 가능하다', () => {
      it('provider findOne 테스트 : with one-to-one relation (User)', async () => {
        try {
          await queryRunner.startTransaction();

          const testUser = await entityManager.save(await makeTestUser());
          const testProvider = await makeTestProvider(testUser);
          await entityManager.save(testProvider);

          const provider = await repository.findOne({
            providerCode: testProvider.providerCode,
          });
          expect(provider).toMatchObject({
            providerCode: testProvider.providerCode,
            providerName: testProvider.providerName,
            status: testProvider.status,
          });

          expect(await provider.user).toMatchObject({
            email: testUser.email,
            name: testUser.name,
          });
        } catch (e) {
          throw e;
        } finally {
          await queryRunner.rollbackTransaction();
        }
      });

      it('커스텀 레포지토리의 트랜잭션 제어 : rollback 후 return undefined', async () => {
        try {
          await queryRunner.startTransaction();
          const testUser = await entityManager.save(await makeTestUser());
          const testProvider = await makeTestProvider(testUser);

          const savedProvider = await repository.save(testProvider);

          expect(
            await repository.findOne({
              providerCode: savedProvider.providerCode,
            }),
          ).not.toBeUndefined();

          await queryRunner.rollbackTransaction();

          expect(
            await repository.findOne({
              providerCode: savedProvider.providerCode,
            }),
          ).toBeUndefined();
        } catch (e) {
          throw e;
        } finally {
          if (queryRunner.isTransactionActive) {
            await queryRunner.rollbackTransaction();
          }
        }
      });
    });
  });
});
