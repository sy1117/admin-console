import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserInputError } from 'apollo-server-core';
import { PinoLogger } from 'nestjs-pino';
import { getConnection } from 'typeorm';
import {
  MaxCredentialError,
  UnauthorizedCredentialClient,
} from '../common/errors/custom.credential.error';
import { User } from '../users/entities/user.entity';
import { CredentialsClientsRepository } from './credentials.clients.repository';
import { CredentialsPmsService } from './credentials.pms.service';
import { CredentialsProvidersRepository } from './credentials.providers.repository';
import { CreateCredentialInput } from './dto/create-credential.Input';
import { CredentialClient } from './entities/credential.client.entity';
import { CredentialClientScope } from './entities/credential.client.scope.entity';
import { CredentialProvider } from './entities/credential.provider.entity';
import { CREDENTIAL_PROVIDER_STATUS } from './enums/credentials.provier.status';
import { CREDENTIAL_SCOPE } from './enums/credentials.scope';
const qs = require('qs');

@Injectable()
export class CredentialsService {
  clientIdPrefix: string;
  maxCredentialCount: number;

  constructor(
    /**services */
    private readonly credentialsPmsService: CredentialsPmsService,
    /**repositories */
    private readonly credentialProvidersRepository: CredentialsProvidersRepository,
    private readonly credentialClientsRepository: CredentialsClientsRepository,
    /**config */
    private readonly configService: ConfigService,
    private readonly log: PinoLogger,
  ) {
    log.setContext(CredentialsService.name);
    this.clientIdPrefix = this.configService.get('CREDENTIAL_PREFIX');
    this.maxCredentialCount = Number(
      this.configService.get('MAX_CREDENTIAL_COUNT'),
    );
  }

  async createCredentialWithProviderIfIsNotExist(
    user: User,
    createCredentialInput: CreateCredentialInput,
  ): Promise<CredentialClient> {
    this.log.info(
      `createCredentialWithProviderIfIsNotExist(%s, %o)`,
      user.email,
      createCredentialInput,
    );

    const queryRunner = getConnection().createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    let newCredentialClient: CredentialClient;
    try {
      let credentialProvider: CredentialProvider = await this.findCredentialProviderByUser(
        user,
      );

      let isExistProvider: boolean = true;
      if (!credentialProvider) {
        this.log.info('Provider 정보 없음. 신규 생성 시작..');
        isExistProvider = false;
        credentialProvider = await this.makeNewProvider(user);
      } else {
        if (
          (await credentialProvider.credentialClients).length >=
          this.maxCredentialCount
        ) {
          this.log.error('Credential 은 10개까지 만들 수 있습니다.');
          throw new MaxCredentialError();
        }
      }

      this.log.info('Provider에 Client 정보 입력..');
      newCredentialClient = await this.makeNewClient(createCredentialInput);
      (await credentialProvider.credentialClients).push(newCredentialClient);

      this.log.info('PMS 호출을 위해 Provider,Client 정보 저장..');
      credentialProvider = await this.credentialProvidersRepository.save(
        credentialProvider,
      );

      newCredentialClient.clientId =
        this.clientIdPrefix + '-' + newCredentialClient.id;
      const {
        providerCode,
        status,
        secret,
      } = await this.createCredentialRequestToPms(
        isExistProvider,
        credentialProvider,
        newCredentialClient,
      );

      credentialProvider.providerCode = providerCode;
      credentialProvider.status = CREDENTIAL_PROVIDER_STATUS[status];
      await this.credentialProvidersRepository.save(credentialProvider);

      this.log.debug(credentialProvider);
      this.log.debug(newCredentialClient);
      newCredentialClient.secret = secret;

      queryRunner.commitTransaction();
    } catch (e) {
      queryRunner.rollbackTransaction();
      throw e;
    } finally {
      queryRunner.release();
    }

    return newCredentialClient;
  }

  private async findCredentialProviderByUser(
    user: User,
  ): Promise<CredentialProvider> {
    this.log.info(`findCredentialProviderByUser(%s)`, user.email);
    const provider = await this.credentialProvidersRepository.findOne({
      where: {
        user: user.id,
      },
    });

    this.log.debug(provider);
    return provider;
  }

  async makeNewProvider(user: User) {
    this.log.info(`makeNewProvider(%s)`, user.email);

    const newProvider = new CredentialProvider();
    newProvider.providerName = user.email;
    newProvider.user = user;
    newProvider.status = CREDENTIAL_PROVIDER_STATUS.INACTIVE;
    newProvider.credentialClients = [];

    this.log.debug(newProvider);
    return newProvider;
  }

  private async makeNewClient(createCredentialInput: CreateCredentialInput) {
    this.log.info(`makeNewClient(%o)`, createCredentialInput);
    const newCredentialClient = new CredentialClient();

    newCredentialClient.credentialName = createCredentialInput.name;
    const scopes = await newCredentialClient.scopes;
    for (let i = 0; i < createCredentialInput.scopes.length; i++) {
      let newScope = new CredentialClientScope();
      newScope.scope = createCredentialInput.scopes[i];
      scopes.push(newScope);
    }

    this.log.debug(newCredentialClient);
    return newCredentialClient;
  }

  private async createCredentialRequestToPms(
    isProviderExist: boolean,
    provider: CredentialProvider,
    client: CredentialClient,
  ): Promise<any> {
    const scopes = await client.scopes;
    let scopesToArray: CREDENTIAL_SCOPE[] = [];
    for (const [i, v] of scopes.entries()) {
      scopesToArray.push(v.scope);
    }

    if (!isProviderExist) {
      const res = await this.credentialsPmsService.createPmsProvider(
        provider.providerName,
        client.clientId,
        scopesToArray,
      );

      res.secret = res.clients[0].secret;
      return res;
    } else {
      return await this.credentialsPmsService.createPmsClient(
        provider.providerCode,
        client.clientId,
        scopesToArray,
      );
    }
  }

  async findCredentialsAreOwnedByUser(user: User): Promise<CredentialClient[]> {
    this.log.info(`findCredentialsAreOwnedByUser(%s)`, user.email);
    const provider = await this.credentialProvidersRepository.findOne({
      user: user,
    });

    if (!provider) {
      this.log.debug(
        'Provider 정보가 없습니다. 해당 사용자는 가지고 있는 Credential 정보가 없습니다.',
      );
      return [];
    }
    this.log.debug(provider);

    return provider.credentialClients;
  }

  async findCredentialIfIsOwnedByUser(
    user: User,
    clientId: string,
  ): Promise<CredentialClient> {
    this.log.info(
      `findCredentialIfIsOwnedByUser(%s, %s)`,
      user.email,
      clientId,
    );

    const queryRunner = getConnection().createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    let client: CredentialClient;
    try {
      client = await this.credentialClientsRepository.findOne({
        clientId: clientId,
      });

      if (!client) {
        this.log.error(
          '잘못된 클라이언트 아이디 입니다. clientId = %s',
          clientId,
        );
        throw new UserInputError('잘못된 클라이언트 아이디 입니다.');
      }

      if (!(await this.isClientIsOwnedByUser(user, clientId))) {
        this.log.error(
          `접근권한이 없는 credential 을 조회했습니다. user: %s, clientId: %s`,
          user.email,
          clientId,
        );

        throw new UnauthorizedCredentialClient();
      }
      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }

    this.log.debug(client);
    return client;
  }

  private async findCredential(clientId: string): Promise<CredentialClient> {
    const client = await this.credentialClientsRepository.findOne({
      clientId: clientId,
    });

    this.log.debug(client);
    return client;
  }

  private async isClientIsOwnedByUser(user: User, clientId: string) {
    this.log.info(`isCredentialIsOwnedByUser(%s, %s)`, user.email, clientId);

    const client = await this.findCredential(clientId);

    const clientUser = await (await client.credentialProvider).user;
    if (clientUser.email !== user.email) {
      return false;
    }

    return true;
  }

  async deleteCredential(user: User, clientId: string): Promise<Boolean> {
    this.log.info(`deleteCredential(%s, %s)`, user.email, clientId);

    const queryRunner = getConnection().createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const client = await this.findCredentialIfIsOwnedByUser(user, clientId);

      const provider = await client.credentialProvider;
      const providerCode = provider.providerCode;
      if (await this.isProviderHasOnlyOneCredential(provider)) {
        this.log.info('마지막 Client 삭제 요청으로 Provider 정보 삭제.');

        await this.credentialProvidersRepository.delete({
          id: provider.id,
        });
        await this.credentialsPmsService.deletePmsProvider(providerCode);
      } else {
        await this.credentialClientsRepository.delete({
          clientId: clientId,
        });
        await this.credentialsPmsService.deletePmsClient(
          providerCode,
          clientId,
        );
      }

      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    } finally {
      await queryRunner.release();
    }

    return true;
  }

  private async isProviderHasOnlyOneCredential(provider: CredentialProvider) {
    this.log.info(`isProviderHasOnlyOneCredential(%o)`, provider);

    if ((await provider.credentialClients).length > 1) {
      return false;
    }
    return true;
  }
}
