import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PinoLogger } from 'nestjs-pino';
import { ExternalApiError } from '../common/errors/custom.external.api.error';
import {
  CreatePmsClientError,
  CreatePmsProviderError,
  CreatePmsTokenError,
  DeletePmsClientError,
  DeletePmsProviderError,
} from '../common/errors/custom.pms.error';
import { CREDENTIAL_SCOPE } from './enums/credentials.scope';
const qs = require('qs');

@Injectable()
export class CredentialsPmsService {
  pmsApiUrl: string;
  keycloakUrl: string;
  serverClientId: string;
  serverClientSecret: string;

  constructor(
    private readonly log: PinoLogger,
    private readonly configService: ConfigService,
    private readonly http: HttpService,
  ) {
    this.log.setContext(CredentialsPmsService.name);
    this.pmsApiUrl = this.configService.get('PMS_API_URL');
    this.keycloakUrl = this.configService.get('KEYCLOAK_URL');
    this.serverClientId = this.configService.get('KEYCLOAK_CLIENT_ID');
    this.serverClientSecret = this.configService.get('KEYCLOAK_SECRET');
  }

  private async makePmsRequestHeader(): Promise<any> {
    this.log.info('makePmsRequestHeader()');
    return {
      'Content-Type': 'application/json',
      authorization: 'Bearer ' + (await this.getAdminAccessToken()),
    };
  }

  private async getAdminAccessToken(): Promise<string> {
    this.log.info('getAdminAccessToken()');

    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    const body = {
      client_id: this.serverClientId,
      client_secret: this.serverClientSecret,
      grant_type: 'client_credentials',
    };

    try {
      const data = await this.postRequest(
        this.keycloakUrl,
        headers,
        qs.stringify(body),
      );

      return data.access_token;
    } catch (e) {
      this.log.error(`PMS API Error. Get Access Token. e : %o`, e);
      throw new CreatePmsTokenError();
    }
  }

  private async postRequest(
    url: string,
    headers: any,
    body: any,
  ): Promise<any> {
    this.log.info('postRequest(%s, %o, %o)', url, headers, body);
    const res = await this.http
      .post(url, body, {
        headers: headers,
      })
      .toPromise()
      .then((res) => {
        if (res.status !== 200) {
          this.log.error(`Status ${res.status} ${res.statusText}`);
          this.log.debug(res);
          throw new ExternalApiError(res.statusText);
        }
        this.log.info('Success');
        this.log.debug(res.data);
        return res.data;
      })
      .catch((e) => {
        this.log.error(`External API Error. e : %o`, e);
        throw e;
      });

    return res;
  }

  private async deleteRequest(url: string, headers: any): Promise<any> {
    const res = await this.http
      .delete(url, {
        headers: headers,
      })
      .toPromise()
      .then((res) => {
        if (res.status !== 200) {
          this.log.error(`Status ${res.status} ${res.statusText}`);
          this.log.debug(res);
          throw new ExternalApiError(res.statusText);
        }
        this.log.info('Success');
        this.log.debug(res.data);
        return res.data;
      })
      .catch((e) => {
        this.log.error(`External API Error. e : %o`, e);
        throw e;
      });
  }

  async createPmsProvider(
    providerName: string,
    clientId: string,
    scopes: CREDENTIAL_SCOPE[],
  ): Promise<any> {
    this.log.info(
      `createPMSProvider(%s,%s,%s)`,
      providerName,
      clientId,
      scopes.toString(),
    );

    const body = {
      name: providerName,
      clientId: clientId,
      scope: scopes.toString(),
    };

    const header = await this.makePmsRequestHeader();

    try {
      const data = await this.postRequest(
        this.pmsApiUrl + '/providers',
        header,
        body,
      );

      return data.result;
    } catch (e) {
      this.log.error(`PMS API Error. Create provider. e : %o`, e);
      throw new CreatePmsProviderError();
    }
  }

  async createPmsClient(
    providerCode: string,
    clientId: string,
    scopes: CREDENTIAL_SCOPE[],
  ): Promise<any> {
    this.log.info(
      `createPMSClient(%s, %s, %s)`,
      providerCode,
      clientId,
      scopes.toString(),
    );

    const body = {
      clientId: clientId,
      scope: scopes.toString(),
    };

    const header = await this.makePmsRequestHeader();

    try {
      const data = await this.postRequest(
        this.pmsApiUrl + '/providers/' + providerCode + '/clients',
        header,
        body,
      );

      return data.result;
    } catch (e) {
      this.log.error(`PMS API Error. Create client. e : %o`, e);
      throw new CreatePmsClientError();
    }
  }

  async deletePmsProvider(providerCode: string): Promise<void> {
    this.log.info(`deletePmsProvider(%s)`, providerCode);

    const header = await this.makePmsRequestHeader();

    try {
      await this.deleteRequest(
        this.pmsApiUrl + '/providers/' + providerCode,
        header,
      );
    } catch (e) {
      this.log.error(`PMS API Error. Delete client. e : %o`, e);
      throw new DeletePmsProviderError();
    }
  }

  async deletePmsClient(providerCode: string, clientId: string): Promise<void> {
    this.log.info(`deletePMSClient(%s,%s)`, providerCode, clientId);

    const header = await this.makePmsRequestHeader();

    try {
      await this.deleteRequest(
        this.pmsApiUrl + '/providers/' + providerCode + '/clients/' + clientId,
        header,
      );
    } catch (e) {
      this.log.error(`PMS API Error. Delete client. e : %o`, e);
      throw new DeletePmsClientError();
    }
  }
}
