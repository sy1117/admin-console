import { EntityRepository, Repository } from 'typeorm';
import { CredentialProvider } from './entities/credential.provider.entity';

@EntityRepository(CredentialProvider)
export class CredentialsProvidersRepository extends Repository<CredentialProvider> {}
