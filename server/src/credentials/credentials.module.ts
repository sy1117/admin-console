import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CredentialsClientsRepository } from './credentials.clients.repository';
import { CredentialsPmsService } from './credentials.pms.service';
import { CredentialsProvidersRepository } from './credentials.providers.repository';
import { CredentialsResolver } from './credentials.resolver';
import { CredentialsService } from './credentials.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CredentialsProvidersRepository,
      CredentialsClientsRepository,
    ]),
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 2000,
        maxRedirects: 5,
      }),
    }),
  ],
  providers: [CredentialsService, CredentialsResolver, CredentialsPmsService],
})
export class CredentialsModule {}
