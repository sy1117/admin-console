import { registerEnumType } from "@nestjs/graphql";

export enum CREDENTIAL_PROVIDER_STATUS {
  ACTIVE = "ACTIVE",
  INACTIVE = "INACTIVE",
}
registerEnumType(CREDENTIAL_PROVIDER_STATUS, {
  name: "CREDENTIAL_PROVIDER_STATUS",
});
