import { registerEnumType } from "@nestjs/graphql";

export enum CREDENTIAL_SCOPE {
  MIDDLEWARE = "mw-scope",
  KRS = "krs-scope",
}
registerEnumType(CREDENTIAL_SCOPE, {
  name: "CREDENTIAL_SCOPE",
});
