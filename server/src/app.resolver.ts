import { Query, Resolver } from '@nestjs/graphql';
@Resolver()
export class AppResolver {
  constructor() {}

  @Query(() => Boolean)
  healthCheck(): Promise<Boolean> {
    return Promise.resolve(true);
  }
}
