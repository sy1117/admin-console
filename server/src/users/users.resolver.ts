import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { DeleteResult } from 'typeorm';
import { AuthenticatedGuard } from '../auth/guards/authenticated.guard';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@Resolver(() => User)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Mutation(() => User)
  createUser(
    @Args('createUserInput') createUserInput: CreateUserInput,
  ): Promise<User> {
    return this.usersService.create(createUserInput);
  }

  @UseGuards(AuthenticatedGuard)
  @Query(() => [User], { nullable: true })
  users(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @UseGuards(AuthenticatedGuard)
  @Query(() => User, {
    nullable: true,
  })
  user(@Args('email', { type: () => String }) email: string): Promise<User> {
    return this.usersService.findByEmail(email);
  }

  // @UseGuards(AuthenticatedGuard)
  // @Query(() => User, { nullable: true })
  // userByEmail(
  //   @Args('email', { type: () => String }) email: string
  // ): Promise<User> {
  //   return this.usersService.findByEmail(email);
  // }

  @UseGuards(AuthenticatedGuard)
  @Mutation(() => User)
  updateUser(
    @Args('email') email: string,
    @Args('updateUserInput') updateUserInput: UpdateUserInput,
  ): Promise<User> {
    return this.usersService.update(email, updateUserInput);
  }

  @UseGuards(AuthenticatedGuard)
  @Mutation(() => User)
  removeUser(
    @Args('email', { type: () => String }) email: string,
  ): Promise<DeleteResult> {
    return this.usersService.delete(email);
  }

  // @Mutation((returns) => LoginOutput)
  // login(@Args('loginInput') loginInput: LoginInput): LoginOutput {
  //   return this.usersService.login(loginInput);
  // }
}
