import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as bcrypt from 'bcrypt';
import { PinoLogger } from 'nestjs-pino';
import { Connection, DeleteResult, getConnection } from 'typeorm';
import { InvalidPasswordError } from '../common/errors/custom.user.error';
import { LockedUserError } from '../common/errors/custom.user.error';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './entities/user.entity';
import { UsersRepository } from './users.repository';

@Injectable()
export class UsersService {
  connection: Connection;

  loginFailCount: number;

  constructor(
    /**repositories */
    private readonly usersRepository: UsersRepository,
    /**config */
    private readonly log: PinoLogger,
    private readonly configService: ConfigService,
  ) {
    log.setContext(UsersService.name);
    this.loginFailCount = Number(this.configService.get('USER_LOCK_COUNT'));
  }

  async findAll(): Promise<User[]> {
    this.log.info('findAll()');
    const users = await this.usersRepository.find();
    this.log.debug('users : %o', users);
    return users;
  }

  async findById(id: number): Promise<User> {
    this.log.info('findById()');
    const user = await this.usersRepository.findOne(id);
    this.log.debug('user : %o', user);
    return user;
  }

  async findByEmail(email: string): Promise<User> {
    this.log.info('findByEmail()');
    const user = await this.usersRepository.findOne({ email });
    return user;
  }

  async create(createUserInput: CreateUserInput): Promise<User> {
    this.log.info(
      'create(). email:%s, name:%s, bio:%s',
      createUserInput.email,
      createUserInput.name,
    );

    const user = new User();
    user.email = createUserInput.email;
    user.name = createUserInput.name;

    user.password = await bcrypt.hash(
      createUserInput.password,
      await bcrypt.genSalt(),
    );

    this.log.info('사용자 정보 저장중..');
    const createdUser = await this.usersRepository.save(user);
    this.log.debug(`createdUser : %o`, createdUser);

    return createdUser;
  }

  async update(email: string, updateUserInput: UpdateUserInput): Promise<User> {
    this.log.info('update(email : %s).', email);
    const queryRunner = getConnection().createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    let updatedUser: User;

    try {
      const user = await this.usersRepository.findOne({ email });
      if (
        !(await bcrypt.compare(updateUserInput.currentPassword, user.password))
      ) {
        this.log.error('패스워드 불일치');
        throw new InvalidPasswordError();
      }

      if (
        updateUserInput.newPassword !== undefined &&
        updateUserInput.newPasswordCheck !== undefined
      ) {
        user.password = await bcrypt.hash(
          updateUserInput.newPassword,
          await bcrypt.genSalt(),
        );
      }

      user.name =
        updateUserInput.name !== undefined ? updateUserInput.name : user.name;

      this.log.info('사용자 정보 업데이트 중..');
      updatedUser = await this.usersRepository.save(user);
      this.log.debug(`updatedUser : %o`, updatedUser);
      queryRunner.commitTransaction();
    } catch (e) {
      queryRunner.rollbackTransaction();
      throw e;
    } finally {
      queryRunner.release();
    }

    return updatedUser;
  }

  async delete(email: string): Promise<DeleteResult> {
    this.log.info('delete(email : %s)', email);
    const deleteResult = await this.usersRepository.delete({ email });
    this.log.debug(`deleteResult : %o`, deleteResult);
    return deleteResult;
  }

  async signinFail(email: string): Promise<void> {
    this.log.info('signinFail()');
    const user = await this.findByEmail(email);
    if (user.signinFailCount < this.loginFailCount) {
      user.signinFailCount += 1;
      await this.usersRepository.save(user);
      this.log.debug(
        `email : ${email}. signinFailCount : ${user.signinFailCount} 잘못된 비밀번호 입력. `,
      );
    } else {
      this.log.info(
        `email : ${email} 사용자 비밀번호 입력 ${user.signinFailCount}회 실패로 계정이 잠겼습니다.`,
      );
      user.isLocked = true;
      await this.usersRepository.save(user);
      throw new LockedUserError(
        `비밀번호 입력 ${user.signinFailCount}회 실패로 계정이 잠겼습니다. 계정을 잠금해제 하시려면 비밀번호 찾기를 진행 해주세요`,
      );
    }
  }
}
