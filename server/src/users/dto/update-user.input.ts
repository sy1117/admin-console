import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, Matches, MaxLength } from 'class-validator';
import { Match } from '../../auth/decorators/field-match.decorator';

@InputType()
export class UpdateUserInput {
  @Field(() => String, { description: 'Currnet password', nullable: false })
  currentPassword!: string;

  @IsOptional()
  @Matches(/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,16}/, {
    message:
      '패스워드는 영문/숫자/특수문자 를 포함해 8~16자 이어야 합니다. 사용가능 특수문자 : !@#$%^*+=-',
  })
  @Field(() => String, { description: 'New password' })
  newPassword?: string;

  @Match('newPassword', { message: '신규 패스워드가 일치하지 않습니다.' })
  @Field(() => String, { description: 'New password check' })
  newPasswordCheck?: string;

  @MaxLength(50)
  @Field(() => String, { description: 'Name' })
  name?: string;
}
