import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, Matches, MaxLength } from 'class-validator';
import { Match } from '../../auth/decorators/field-match.decorator';

@InputType()
export class CreateUserInput {
  @MaxLength(100)
  @IsEmail()
  @Field(() => String, { description: 'Email', nullable: false })
  email!: string;

  @Field(() => String, { description: 'Password', nullable: false })
  @Matches(/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,16}/, {
    message:
      '패스워드는 영문/숫자/특수문자 를 포함해 8~16자 이어야 합니다. 사용가능 특수문자 : !@#$%^*+=-',
  })
  password!: string;

  @Match('password', { message: '패스워드가 일치하지 않습니다.' })
  @Field(() => String, { description: 'Password Check', nullable: false })
  passwordCheck!: string;

  @MaxLength(50)
  @Field(() => String, { description: 'Name', nullable: false })
  name!: string;
}
