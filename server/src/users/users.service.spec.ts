import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { LoggerModule } from 'nestjs-pino';
import { Connection, QueryRunner } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { InvalidPasswordError } from '../common/errors/custom.user.error';
import { DBConfigModule } from '../config/db.config.module';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User } from './entities/user.entity';
import { UsersRepository } from './users.repository';
import { UsersService } from './users.service';

/**
 * TODO
 * 사용자 api관련 목록이 명확해지면 서비스 정리와 함께 테스트코드 보완할것.
 */
describe('UsersService', () => {
  let service: UsersService;
  let repository: UsersRepository;

  let queryRunner: QueryRunner;
  let connection: Connection;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: `.env.test`,
          isGlobal: true,
        }),
        DBConfigModule,
        TypeOrmModule.forFeature([UsersRepository]),
        LoggerModule.forRoot(),
      ],
      providers: [UsersService, ConfigService],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repository = module.get<UsersRepository>(UsersRepository);

    connection = module.get<Connection>(Connection);

    queryRunner = connection.createQueryRunner('master');
    await queryRunner.connect();
  });

  afterAll(async () => {
    await queryRunner.connection.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('테스트 데이터 및 트랜잭션 설정', () => {
    const makeTestUserWithParams = async (
      email: string,
      name: string,
      password: string,
    ) => {
      const user = new User();
      user.name = name;
      user.email = email;
      user.password = await bcrypt.hash(password, await bcrypt.genSalt());

      return user;
    };

    describe('User 생성', () => {
      describe('create(createUserInput)', () => {
        it('1. User를 하나 생성한다.', async () => {
          const createUserInput = new CreateUserInput();
          createUserInput.email = uuidv4() + '@sk.com';
          createUserInput.name = 'tester';
          createUserInput.password = '1234';
          createUserInput.passwordCheck = '1234';

          const mockUser = await makeTestUserWithParams(
            createUserInput.email,
            createUserInput.name,
            createUserInput.password,
          );

          jest
            .spyOn(repository, 'save')
            .mockImplementationOnce(() => Promise.resolve(mockUser));

          const createdUser = await service.create(createUserInput);

          expect(createdUser).toMatchObject({
            email: createUserInput.email,
            name: createUserInput.name,
          });

          expect(
            await bcrypt.compare(
              createUserInput.password,
              createdUser.password,
            ),
          ).toEqual(true);
        });
      });
    });

    describe('User 업데이트', () => {
      describe('update(email, updateUserInput)', () => {
        it('1. User 정보를 업데이트 한다.', async () => {
          const oldPassword = '1234';
          const mockUser = await makeTestUserWithParams(
            uuidv4() + '@sk.com',
            'tester',
            oldPassword,
          );

          const updateUserInput = new UpdateUserInput();
          updateUserInput.name = 'tester2';
          updateUserInput.currentPassword = oldPassword;
          updateUserInput.newPassword = '12345';
          updateUserInput.newPasswordCheck = '12345';

          jest
            .spyOn(repository, 'findOne')
            .mockImplementationOnce(() => Promise.resolve(mockUser));

          const updatedUser = await service.update(
            mockUser.email,
            updateUserInput,
          );

          expect(updatedUser).toMatchObject({
            email: mockUser.email,
            name: updateUserInput.name,
          });

          expect(
            await bcrypt.compare(
              updateUserInput.newPassword,
              updatedUser.password,
            ),
          ).toEqual(true);
        });

        it('2. 패스워드가 일치하지 않으면 throw InvalidPasswordErrr 한다.', async () => {
          const oldPassword = '1234';
          const mockUser = await makeTestUserWithParams(
            'asdf@sk.com',
            'tester',
            oldPassword,
          );

          const updateUserInput = new UpdateUserInput();
          updateUserInput.name = 'tester2';
          updateUserInput.currentPassword = oldPassword + 'INVALID';
          updateUserInput.newPassword = '12345';
          updateUserInput.newPasswordCheck = '12345';

          jest
            .spyOn(repository, 'findOne')
            .mockImplementationOnce(() => Promise.resolve(mockUser));

          await expect(
            service.update(mockUser.email, updateUserInput),
          ).rejects.toThrow(InvalidPasswordError);
        });
      });
    });
  });
});
