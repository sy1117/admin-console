import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, Index, OneToOne } from 'typeorm';
import CoreEntity from '../../common/entities/core.entity';
import { CredentialProvider } from '../../credentials/entities/credential.provider.entity';

@Entity()
@ObjectType()
export class User extends CoreEntity {
  @Column()
  @Index({ unique: true })
  @Field(() => String, {
    nullable: false,
  })
  email!: string;

  @Column()
  password!: string;

  @Column()
  @Field(() => String, {
    nullable: false,
  })
  name!: string;

  @Column({ default: 0 })
  @Field(() => Number, {
    nullable: false,
  })
  signinFailCount!: number;

  @Column({ default: false })
  @Field(() => Boolean, {
    nullable: false,
  })
  isLocked!: boolean;

  @OneToOne(
    () => CredentialProvider,
    (credentialProvider) => credentialProvider.user,
    {
      nullable: true,
      cascade: false,
    },
  )
  @Field(() => CredentialProvider)
  credentialProvider?: CredentialProvider;
}
