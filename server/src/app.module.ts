import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppResolver } from './app.resolver';
import { AuthModule } from './auth/auth.module';
import { CommonConfigModule } from './config/common.config.module';
import { CredentialsModule } from './credentials/credentials.module';
import { UsersModule } from './users/users.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: !process.env.NODE_ENV
        ? '.env.dev'
        : `.env.${process.env.NODE_ENV}`,
      isGlobal: true,
    }),
    CommonConfigModule,
    UsersModule,
    AuthModule,
    CredentialsModule,
  ],
  providers: [AppResolver],
})
export class AppModule {}
