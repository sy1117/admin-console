import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { v4 as uuidv4 } from 'uuid';
import { AppModule } from '../src/app.module';
import { CreateUserInput } from '../src/users/dto/create-user.input';

describe('API (e2e)', () => {
  let app: INestApplication;
  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  const testUserEmail: string = uuidv4() + '@sk.com';
  const testUserPassword: string = '1q2w3e4r!@';
  let session: string;

  describe('App API', () => {
    it('health check api', async (done) => {
      request(app.getHttpServer())
        .post('/graphql')
        .send({
          query: '{ healthCheck }',
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.healthCheck).toEqual(true);
        })
        .end(done);
    });
  });

  describe('User API', () => {
    const makeCreateUserInput = () => {
      const createUserInput = new CreateUserInput();
      createUserInput.email = testUserEmail;
      createUserInput.name = 'tester';
      createUserInput.password = testUserPassword;
      createUserInput.passwordCheck = createUserInput.password;

      return createUserInput;
    };

    it('create user', async (done) => {
      const createUserInput = makeCreateUserInput();

      await request(app.getHttpServer())
        .post('/graphql')
        .send({
          query: `mutation {
          createUser(createUserInput: {
            email: "${createUserInput.email}",
            name: "${createUserInput.name}",
            password: "${createUserInput.password}",
            passwordCheck: "${createUserInput.passwordCheck}",
          }) {
            email, name, signinFailCount, isLocked, createdAt, updatedAt
          }
        }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.createUser.email).toEqual(createUserInput.email);
          expect(res.body.data.createUser.name).toEqual(createUserInput.name);
          expect(res.body.data.createUser.signinFailCount).toEqual(0);
          expect(res.body.data.createUser.isLocked).toEqual(false);
          expect(res.body.data.createUser.createdAt).not.toBeNull();
          expect(res.body.data.createUser.updatedAt).not.toBeNull();
        });

      done();
    });

    it('login', async (done) => {
      await request(app.getHttpServer())
        .post('/graphql')
        .send({
          query: `mutation { 
          login(email: "${testUserEmail}", password: "${testUserPassword}") {
            email, name, signinFailCount, isLocked, createdAt, updatedAt
          }
        }`,
        })
        .expect(200)
        .expect((res) => {
          const setCookies: string = res.headers['set-cookie'][0].split(';');
          for (let i = 0; i < setCookies.length; i++) {
            if (setCookies[i].includes('connect.sid')) {
              session = setCookies[i];
            }
          }
          expect(session).not.toBeUndefined();
          expect(res.body.data.login.email).toEqual(testUserEmail);
          expect(res.body.data.login.name).not.toBeNull();
          expect(res.body.data.login.signinFailCount).toEqual(0);
          expect(res.body.data.login.isLocked).toEqual(false);
          expect(res.body.data.login.createdAt).not.toBeNull();
          expect(res.body.data.login.updatedAt).not.toBeNull();
        });

      done();
    });

    it('find user', async (done) => {
      await request(app.getHttpServer())
        .post('/graphql')
        .set('Cookie', session)
        .send({
          query: `{
        users {
            email, name, signinFailCount, isLocked, createdAt, updatedAt
        }
      }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.users.length).not.toEqual(0);
          expect(res.body.data.users[0].email).not.toBeNull();
          expect(res.body.data.users[0].name).not.toBeNull();
          expect(res.body.data.users[0].signinFailCount).not.toBeNull();
          expect(res.body.data.users[0].isLocked).not.toBeNull();
          expect(res.body.data.users[0].createdAt).not.toBeNull();
          expect(res.body.data.users[0].updatedAt).not.toBeNull();
        });
      done();
    });
  });

  describe('Credentials API', () => {
    let testCredentialId: string;
    const testCredentialName: string = uuidv4();

    it('credential 생성', async (done) => {
      await request(app.getHttpServer())
        .post('/graphql')
        .set('Cookie', session)
        .send({
          query: `mutation{
            createCredential(createCredentialInput:{
              name:"${testCredentialName}",
              scopes:[MIDDLEWARE,KRS],
            }) {
              credentialName,
              secret,
              clientId,
              scopes{
                scope
              },
              createdAt,
              updatedAt,
            }
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.createCredential.clientId).not.toBeNull();
          expect(res.body.data.createCredential.credentialName).toEqual(
            testCredentialName,
          );
          expect(res.body.data.createCredential.secret).not.toBeNull();
          expect(res.body.data.createCredential.scopes.length).toEqual(2);
          expect(res.body.data.createCredential.createdAt).not.toBeNull();
          expect(res.body.data.createCredential.updatedAt).not.toBeNull();

          testCredentialId = res.body.data.createCredential.clientId;
        });
      done();
    });

    it('credential 목록 조회', async (done) => {
      await request(app.getHttpServer())
        .post('/graphql')
        .set('Cookie', session)
        .send({
          query: `{
             credentials {
              credentialName,
              clientId,
              scopes{
                scope
              },
              createdAt,
              updatedAt,
            }
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.credentials.length).not.toEqual(0);
          expect(res.body.data.credentials[0].clientId).not.toBeNull();
          expect(res.body.data.credentials[0].credentialName).toEqual(
            testCredentialName,
          );
          expect(res.body.data.credentials[0].scopes.length).toEqual(2);
          expect(res.body.data.credentials[0].createdAt).not.toBeNull();
          expect(res.body.data.credentials[0].updatedAt).not.toBeNull();
        });
      done();
    });

    it('credential 단건 조회', async (done) => {
      await request(app.getHttpServer())
        .post('/graphql')
        .set('Cookie', session)
        .send({
          query: `{
             credential(clientId:"${testCredentialId}") {
              credentialName,
              clientId,
              scopes{
                scope
              },
              createdAt,
              updatedAt,
            }
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.credential.clientId).not.toBeNull();
          expect(res.body.data.credential.credentialName).toEqual(
            testCredentialName,
          );
          expect(res.body.data.credential.scopes.length).toEqual(2);
          expect(res.body.data.credential.createdAt).not.toBeNull();
          expect(res.body.data.credential.updatedAt).not.toBeNull();
        });
      done();
    });

    it('credential 삭제', async (done) => {
      await request(app.getHttpServer())
        .post('/graphql')
        .set('Cookie', session)
        .send({
          query: `mutation {
            deleteCredential(clientId:"${testCredentialId}")
          }`,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.data.deleteCredential).toEqual(true);
        });
      done();
    });
  });
});
