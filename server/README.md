# ChainZ Admin Console BE

## Description

## Installation

```bash
$yarn install
```

# DB 설정

docker 실행시 아래 명령어 참조

```bash
$docker run --name adminconsole -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1234 mariadb
```

1. .env (.envdev) 편집
2. database 생성 (CREATE_DATABASE adminconsole)
3. 서버 실행시 자동으로 db table 동기화

## Session (Redis) 설정

.env (.envdev) 내 REDIS_ENABLE=false 로 변경시 Inmemory store 에 세션 저장합니다. 기본값은 true (Redis)입니다.
Inmemory 로 사용시, 서버를 재기동하면 세션정보가 사라지지만 별도 Redis 세팅을 필요로 하지 않기 떄문에 개발중에는 원하는 대로 설정하세요.

```bash
REDIS_ENABLE=false  # true : Redis, false : InmemoryDB
```

### Redis 사용시

1. redis 기동.
   docker 로 기동시 아래 명령어 실행

```bash
$docker run --name redis -d -p 6379:6379 redis redis-server --requirepass "my_password"
```

2. .env (.env.dev) 내 아래 필드 입력. (현재 개발환경에서는 아래 모든값을 정상적으로 입력 해 주어야 합니다.)

```bash
REDIS_ENABLE=true
REDIS_HOST="localhost"
REDIS_PORT=6379
REDIS_PASSWORD="my_password"
REDIS_PREFIX="adminconsole:"
SESSION_SECRET="TEST_SECRET"
SESSION_MAX_AGE=300000
```

## Running the app

```bash
# development
$yarn start:dev
```

# Test

unit 테스트용 db를 세팅이 필요하다. 개발용 db가 이미 있다면 schema(database)를 하나 추가해준다.
table 은 단위테스트 수행시 자동으로 generate 된다. (synchronize : true)

ex) CREATE DATABASE t_adminconsole;

## unit & e2e test 공통 환경변수 편집.

.env.test 파일 내 환경변수 편집 (DB 관련)

## unit test

```bash
# unit test
$yarn test
# jest --coverage
$yarn test:cov
$yarn test --coverage
# 파일경로 지정
$yarn test {filepath}
```

## e2e test

```bash
# e2e tests
$yarn test:e2e
# e2e --coverage
$yarn test:e2e --coverage
```
