# `@adminconsole/codegen`

## Usage
```command
yarn generate
``` 
GraphQL 스키마를 기반으로 타입 인터페이스를 생성한다.

- 서버 실행 시, GraphQLModule autoSchemaFile 옵션을 통해 `schema.gql` 파일이 생성된다. 
  ```ts
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/_generated/schema.gql'),
    }),
  ```
  -> *명령어 수행 전에 한번은 서버를 실행 시켜야 한다는 문제점이 있다.*
- schema.gql 이 생성된 후, 위 명령어를 수행하면 server, client 의 gql 파일을 읽어 타입 인터페이스로 만들어 준다. 
  - [codegen.yml](./codegen.yml) : codegen 설정 파일 
#### Code Generator 를 사용하는 이유?
  - 코드 가이드 및 자동 완성이 되므로, 개발 생산성에 도움이된다. 
  - 스키마와 쿼리가 일치하지 않으면 오류가 발생하여, 스키마와 쿼리의 불일치를 사전에 방지할 수 있다.
 