module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  env: {
    browser: true,
  },
  extends: [
    'airbnb',
    'plugin:prettier/recommended',
    'eslint:recommended',
    'react-app',
  ],
  rules: {
    '@typescript-eslint/no-unused-vars': 'off',
    'import/newline-after-import': 'off',
    'prettier/prettier': 'off',
    camelcase: 'off',
    'no-unused-vars': 'off',
    'no-undef': 'off',
    'react/react-in-jsx-scope': 'off',
    'react/destructuring-assignment': 'off',
    'react/prop-types': 'off',
    'react/jsx-filename-extension': [
      1,
      { extensions: ['.js', '.jsx', '.tsx', '.ts'] },
    ],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'no-shadow': 'off',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'react/no-array-index-key': 'warn',
    'react/jsx-props-no-spreading': 'warn',
    'consistent-return': 'off',
  },
  ignorePatterns: [
    './node_modules/**',
    './dist',
    '**/build',
    'src/_generated/*',
    '**/*.test.*',
  ],
};
