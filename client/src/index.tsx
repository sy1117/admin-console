import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { ApolloProvider } from '@apollo/client';
import { ConfigProvider } from 'antd';
import Router from './router/Router';
import { client } from './lib/apollo';
import 'antd/dist/antd.css';
import validateMessages from './lib/messages/validationMessages';

ReactDOM.render(
  <ApolloProvider client={client}>
    <ConfigProvider form={{ validateMessages }}>
      <Router />
    </ConfigProvider>
  </ApolloProvider>,
  document.getElementById('root'),
);
