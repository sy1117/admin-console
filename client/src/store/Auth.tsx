import { makeVar, ReactiveVar } from '@apollo/client';
import { User } from 'src/_generated/models';

type IUser = Pick<User, 'email' | 'name'>;

export interface IAuth {
  isLoggedIn: boolean;
  user?: IUser;
}

const INITIAL_AUTH: IAuth = {
  isLoggedIn: false,
};

export const authVar: ReactiveVar<IAuth> = makeVar(INITIAL_AUTH);

export const logout = () => {
  authVar({
    isLoggedIn: false,
  });
};
