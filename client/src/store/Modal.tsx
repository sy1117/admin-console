import { makeVar, ReactiveVar, useReactiveVar } from '@apollo/client';
import Modal from 'antd/lib/modal/Modal';
import React from 'react';

interface IModal {
  isOpen: boolean;
  modalContent?: React.ReactElement;
  onOk?: any;
  onCancel?: any;
}
const INITIAL_MODAL = {
  isOpen: false,
};

export const modalVar: ReactiveVar<IModal> = makeVar(INITIAL_MODAL);

export const showModal = () => {
  modalVar({
    isOpen: true,
  });
};

const Overlay: React.FC = ({ children }) => {
  const { isOpen } = useReactiveVar(modalVar);
  console.log(isOpen);
  return <Modal visible={isOpen}>test</Modal>;
};

export default Overlay;
