import '@testing-library/react';
import {
  CreateCredentialDocument,
  CredentialsDocument,
  DeleteCredentialDocument,
} from 'src/_generated/models';
import { renderWithRouter } from 'src/lib/test-utils';
import CredentialPage from '../pages/CredentialPage';
import {
  fireEvent,
  getByDisplayValue,
  getByLabelText,
  getByText,
  screen,
  waitFor,
} from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { convertDatetime } from 'src/lib/datetime';
import saveAsFile from 'src/lib/saveAsFile';
import confirmMessages from 'src/lib/messages/confirmMessages';

jest.mock('../lib/saveAsFile');
let saveAsFileMock = jest.genMockFromModule<any>('../lib/saveAsFile').default;
saveAsFileMock = jest.fn();

const credentials = [
  {
    scopes: [
      {
        scope: 'MIDDLEWARE',
      },
    ],
    clientId: 'client_id_1',
    createdAt: '2021-03-28T15:57:27.064Z',
    credentialName: 'credential_name_1',
  },
];

const getCredentialsMock = {
  request: {
    query: CredentialsDocument,
  },
  result: {
    data: {
      credentials,
    },
  },
};

test('render', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [getCredentialsMock],
  });

  await waitFor(() => {
    return container.getByText(
      getCredentialsMock.result.data.credentials[0].clientId,
    );
  });

  expect(
    container.getByText(getCredentialsMock.result.data.credentials[0].clientId),
  ).toBeInTheDocument();
  let datetime = convertDatetime(
    getCredentialsMock.result.data.credentials[0].createdAt,
  );
  expect(container.getByText(datetime)).toBeInTheDocument();
  expect(
    container.getByText(
      getCredentialsMock.result.data.credentials[0].credentialName,
    ),
  ).toBeInTheDocument();

  expect(container.getByText('Middleware')).toBeInTheDocument();
});
const createCredential = {
  scopes: [
    {
      scope: 'MIDDLEWARE',
    },
  ],
  createdAt: '2021-03-28T11:57:27.064Z',
  clientId: 'NEW_CREDENTIAL_ID',
  credentialName: 'NEW_CREDENTIAL_NAME',
  secret: 'NEW_CREDENTAIL_SECRET',
};

const createCredentialMock = {
  request: {
    query: CreateCredentialDocument,
    variables: {
      name: createCredential.credentialName,
      scopes: createCredential.scopes.map(({ scope }: any) => scope),
    },
  },
  result: {
    data: {
      createCredential,
    },
  },
};

test('create credential & download file ', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [getCredentialsMock, getCredentialsMock, createCredentialMock],
  });

  const createButton = container.getByText('+ Credential 생성');
  fireEvent.click(createButton);
  const inputName = await waitFor(() => screen.getByLabelText('Name'));
  fireEvent.change(inputName, {
    target: { value: createCredential.credentialName },
  });
  const inputScopeMiddleware = screen.getByLabelText('Middleware');
  await fireEvent.click(inputScopeMiddleware);
  const buttonSubmit = screen.getByText('생성');
  act(() => {
    fireEvent.click(buttonSubmit);
  });

  const clientSecretKey = await waitFor(() =>
    screen.getByLabelText('Client Secret Key'),
  );

  expect(
    screen.getByDisplayValue(
      createCredentialMock.result.data.createCredential.clientId,
    ),
  ).toBeInTheDocument();
  screen.getByDisplayValue(
    createCredentialMock.result.data.createCredential.secret,
  );
  const buttonDownload = screen.getByText('다운로드');

  await waitFor(() => {
    fireEvent.click(buttonDownload);
  });

  expect(saveAsFile).toBeCalledTimes(1);
  expect(saveAsFile).toBeCalledWith(
    {
      clientId: createCredential.clientId,
      scopes: createCredential.scopes,
      secret: createCredential.secret,
    },
    `chainz-credential-${createCredential.clientId}`,
  );
});

test('show/hide credential detail', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [getCredentialsMock],
  });
  const currentCredential = getCredentialsMock.result.data.credentials[0];
  const credentialName = await waitFor(() =>
    container.getByText(currentCredential.credentialName),
  );

  await fireEvent.click(credentialName);
  const dialog = await waitFor(() => screen.getByRole('dialog'));
  const detailName = await waitFor(() => screen.getByLabelText('Name'));
  expect((detailName as any).value).toEqual(currentCredential.credentialName);

  const clientId = screen.getByLabelText('Client Id');
  expect((clientId as any).value).toEqual(currentCredential.clientId);

  getByText(dialog, 'Middleware');

  const createdTime = screen.getByLabelText('Created Time');
  expect((createdTime as any).value).toEqual(
    convertDatetime(currentCredential.createdAt),
  );

  const buttonClose = await waitFor(() => screen.getByText('닫기'));
  await fireEvent.click(buttonClose);
  await waitFor(() => {
    expect(dialog).not.toBeInTheDocument();
  });
});

const errorMessage = 'ERROR_MESSAGE';
const createCredentialErrorMock = {
  request: {
    query: CreateCredentialDocument,
    variables: {
      name: createCredential.credentialName,
      scopes: createCredential.scopes.map(({ scope }: any) => scope),
    },
  },
  error: new Error(errorMessage),
};

test('create credential - throw error ', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [createCredentialErrorMock],
  });
  const createButton = container.getByText('+ Credential 생성');
  fireEvent.click(createButton);
  const buttonSubmit = screen.getByText('생성');

  const inputName = await waitFor(() => screen.getByLabelText('Name'));
  fireEvent.change(inputName, {
    target: { value: createCredential.credentialName },
  });
  const inputScopeMiddleware = screen.getByLabelText('Middleware');
  await fireEvent.click(inputScopeMiddleware);
  act(() => {
    fireEvent.click(buttonSubmit);
  });

  await waitFor(() => screen.getByText(errorMessage));
});

test('show/hide create credential modal ', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [createCredentialMock],
  });
  const createButton = container.getByText('+ Credential 생성');
  fireEvent.click(createButton);
  const buttonSubmit = screen.getByText('생성');
  await fireEvent.click(buttonSubmit);

  const inputName = await waitFor(() => screen.getByLabelText('Name'));
  fireEvent.change(inputName, {
    target: { value: createCredential.credentialName },
  });
  const inputScopeMiddleware = screen.getByLabelText('Middleware');
  await fireEvent.click(inputScopeMiddleware);

  const buttonClose = screen.getByText('닫기');
  await fireEvent.click(buttonClose);

  // hide & show 폼 초기화
  await fireEvent.click(createButton);
  const newInputName = await waitFor(() => screen.getByLabelText('Name'));
  expect(newInputName.nodeValue).toBeNull();
});

test('create credential - validation check', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [createCredentialMock],
  });
  const createButton = container.getByText('+ Credential 생성');
  fireEvent.click(createButton);
  const buttonSubmit = screen.getByText('생성');
  await fireEvent.click(buttonSubmit);
  let label = 'Name';
  await waitFor(() => screen.getAllByRole('alert'));
  const invalidMessages = screen.getAllByRole('alert');
  expect(invalidMessages.length).toBe(2);
  screen.getByText("'Name' 을/를 입력해주세요");
  screen.getByText("'Scopes' 을/를 입력해주세요");
});

const deleteCredentialMock = {
  request: {
    query: DeleteCredentialDocument,
    variables: {
      clientId: getCredentialsMock.result.data.credentials[0].clientId,
    },
  },
  result: {
    data: {
      deleteCredential: true,
    },
  },
};

test('delete credential', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [getCredentialsMock, deleteCredentialMock, getCredentialsMock],
  });

  const deleteClientId = getCredentialsMock.result.data.credentials[0].clientId;
  // wait for rendering
  await waitFor(() => {
    return container.getByText(deleteClientId);
  });

  const deleteButton = screen.getByRole('delete');
  await fireEvent.click(deleteButton);

  const deleteConfirmButton = await waitFor(() => container.getByText('삭제'));
  await act(async () => {
    await fireEvent.click(deleteConfirmButton);
  });

  await waitFor(() =>
    screen.getByText(
      confirmMessages({ clientId: deleteClientId }).credentialDeleted,
    ),
  );
});

const deleteCredentialError = 'DELETE_CREDENTIAL_ERROR';
const deleteCredentialErrorMock = {
  request: {
    query: DeleteCredentialDocument,
    variables: {
      clientId: getCredentialsMock.result.data.credentials[0].clientId,
    },
  },
  error: new Error(deleteCredentialError),
};

test('delete credential - throw Error ', async () => {
  let container = renderWithRouter(<CredentialPage />, {
    route: '/security/credential',
    mocks: [getCredentialsMock, deleteCredentialErrorMock, getCredentialsMock],
  });

  const deleteClientId = getCredentialsMock.result.data.credentials[0].clientId;
  await waitFor(() => {
    return container.getByText(deleteClientId);
  });

  const deleteButton = screen.getByRole('delete');
  await fireEvent.click(deleteButton);

  const deleteConfirmButton = await waitFor(() => container.getByText('삭제'));
  await fireEvent.click(deleteConfirmButton);
  await waitFor(() =>
    screen.getByText(
      confirmMessages({ clientId: deleteClientId }).credentialDeletedFailed,
    ),
  );
});
