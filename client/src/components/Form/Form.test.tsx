import React from 'react';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Form from './Form';

describe('Form Component', () => {
  it('render field type=[text]', () => {
    const fields: any = [
      {
        type: 'text',
        name: 'userName',
        placeholder: 'placeholder',
      },
    ];
    const { container } = render(<Form fields={fields} />);
    const inputEl = container.querySelector(`[name=${fields[0].name}]`);
    expect(inputEl?.tagName).toBe('INPUT');
    expect(inputEl?.getAttribute('type')).toBe('text');
    expect(inputEl?.getAttribute('placeholder')).toBe(fields[0].placeholder);

    if (inputEl) {
      userEvent.type(inputEl, 'Hello, World!');
      expect(inputEl).toHaveValue('Hello, World!');
    }
  });
});
