import React from 'react';
import { Radio, Select } from 'antd';
import Form, { FormItemProps, FormProps, Rule } from 'antd/lib/form';
import { LabelTooltipType } from 'antd/lib/form/FormItemLabel';
import { SizeType } from 'antd/lib/config-provider/SizeContext';
import FieldCheckboxGroup, {
  IFieldCheckboxGroup,
} from './Field/FieldCheckboxGroup';
import FieldRadioGroupButton, {
  IFieldRadioGroupOptions,
} from './Field/FieldRadioGroupButton';
import FieldCheckbox from './Field/FieldCheckbox';
import FieldScopes, { IFieldScopes } from './Field/FieldScopes';
import FieldDateTime, { IFieldDateTime } from './Field/FieldDateTime';
import FieldText, { IFieldText } from './Field/FieldText';
import FieldSecretKey, { IFieldSecretKey } from './Field/FieldSecretKey';
import FieldPassword, { IFieldPassword } from './Field/FieldPassword';
import FieldSubmit, { IFieldSubmit } from './Field/FieldSubmit';
import styles from './Form.module.scss';

export interface IFieldBase extends FormItemProps {
  readOnly?: boolean;
  rules?: Rule[];
  tooltip?: LabelTooltipType;
  placeholder?: string;
  name: string;
  label?: string;
  size?: SizeType;
}
export interface IFieldSelect extends IFieldBase {
  type: 'select';
}

export interface IFieldRadio extends IFieldBase {
  type: 'radio';
}

export interface IFieldCheckbox extends IFieldBase {
  type: 'checkbox';
}

export interface IFieldRender extends IFieldBase {
  type: 'render';
  render: any;
}

export type TField =
  | IFieldText
  | IFieldPassword
  | IFieldRadio
  | IFieldRadioGroupOptions
  | IFieldCheckboxGroup
  | IFieldCheckbox
  | IFieldSelect
  | IFieldScopes
  | IFieldDateTime
  | IFieldSecretKey
  | IFieldRender
  | IFieldSubmit;

export type IField<T> = TField & { name?: keyof T };
export interface IFields<T> extends Array<IField<T>> {}

const getFormComponent = (field: TField) => {
  const { type } = field;
  switch (type) {
    case 'text':
      return FieldText;
    case 'submit':
      return FieldSubmit;
    case 'checkbox':
      return FieldCheckbox;
    case 'checkboxgroup':
      return FieldCheckboxGroup;
    case 'radio':
      return Radio;
    case 'radiogroupbutton':
      return FieldRadioGroupButton;
    case 'select':
      return Select;
    case 'scopes':
      return FieldScopes;
    case 'datetime':
      return FieldDateTime;
    case 'secretkey':
      return FieldSecretKey;
    case 'password':
      return FieldPassword;
  }
};

export const Field: React.FC<IField<any>> = ({ children, ...field }) => {
  const { type, label, rules, tooltip, name, hidden, size } = field;
  const Component =
    type === 'render'
      ? (field as IFieldRender).render
      : getFormComponent(field);

  const className = `${styles.form_field} ${styles[`form_field_${size}`]}`;

  if (!name) {
    return (
      <Form.Item
        className={`${styles.form_item} ${
          type === 'submit' && styles.form_item_submit
        }`}
      >
        <Component {...field} className={className} />
      </Form.Item>
    );
  }

  return (
    <Form.Item
      label={label}
      rules={rules}
      tooltip={tooltip}
      name={name as string}
      hidden={hidden}
      className={styles.form_item}
    >
      <Component {...field} className={className} />
    </Form.Item>
  );
};

export interface IFormController
  extends Pick<
    FormProps,
    | 'form'
    | 'name'
    | 'initialValues'
    | 'onValuesChange'
    | 'layout'
    | 'requiredMark'
    | 'onFinish'
  > {
  fields: IFields<any>;
  hidden?: boolean;
  readonly?: true;
}

export const FormController: React.FC<IFormController> = ({
  form,
  name,
  initialValues,
  onValuesChange,
  layout = 'vertical',
  requiredMark = true,
  onFinish,
  fields = [],
  children,
}) => (
  <Form
    form={form}
    layout={layout}
    initialValues={initialValues}
    onValuesChange={onValuesChange}
    onFinish={onFinish}
    requiredMark={requiredMark}
    name={name}
    className={styles.form_root}
  >
    {fields.map((fieldProps, idx) => (
      <Field {...fieldProps} key={idx} />
    ))}
    {children}
  </Form>
);

export default FormController;
export const { useForm } = Form;
