import { FormInstance, RequiredMark } from 'antd/lib/form/Form';
import { InfoCircleOutlined } from '@ant-design/icons';
import React, { useEffect, useRef, useState } from 'react';
import { Modal } from 'antd';
import Form, { useForm } from './Form';

interface ModalFormProps {
  visible: boolean;
  onCancel: () => void;
}

const useResetFormOnCloseModal = ({
  form,
  visible,
}: {
  form: FormInstance;
  visible: boolean;
}) => {
  const prevVisibleRef = useRef<boolean>();
  useEffect(() => {
    prevVisibleRef.current = visible;
  }, [visible]);
  const prevVisible = prevVisibleRef.current;

  useEffect(() => {
    if (!visible && prevVisible) {
      form.resetFields();
    }
  }, [visible]);
};

enum STEP {
  INIT,
  CREATED,
}

const IPWhitelistForm: React.FC<ModalFormProps> = ({ visible, onCancel }) => {
  const [step, setStep] = useState<number>(STEP.INIT);
  const [form] = useForm();
  const [requiredMark, setRequiredMarkType] = useState<RequiredMark>(
    'optional',
  );

  useResetFormOnCloseModal({
    form,
    visible,
  });

  const onRequiredTypeChange = ({
    requiredMarkValue,
  }: {
    requiredMarkValue: RequiredMark;
  }) => {
    setRequiredMarkType(requiredMarkValue);
  };

  const onOk = async () => {
    const result = await form.validateFields();
    if (step === STEP.INIT && result) {
      setStep(STEP.CREATED);
    }
  };

  return (
    <Modal
      title="Basic Drawer"
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
    >
      <Form
        form={form}
        layout="vertical"
        initialValues={{ requiredMark }}
        onValuesChange={onRequiredTypeChange}
        requiredMark={requiredMark}
        name="whitelistForm"
        fields={[
          {
            type: 'text',
            label: 'IpAddress',
            rules: [{ required: true }],
            tooltip: 'This is a required field',
            name: 'ipAddress',
          },
          {
            type: 'text',
            label: 'secretKey',
            rules: [{ required: false }],
            tooltip: 'This is a optional field',
            name: 'secretKey',
          },
        ]}
      />
    </Modal>
  );
};

export default IPWhitelistForm;
