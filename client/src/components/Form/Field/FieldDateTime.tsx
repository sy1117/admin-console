import { Input, InputProps } from 'antd';
import React from 'react';
import { convertDatetime } from 'src/lib/datetime';
import { IFieldBase } from '../Form';

export interface IFieldDateTime
  extends InputProps,
    Omit<IFieldBase, 'name' | 'children' | 'onReset'> {
  type: 'datetime';
}

const FieldDateTime: React.FC<IFieldDateTime> = ({
  type,
  //   readOnly,
  value,
  ...props
}) => {
  const formattedValue = convertDatetime(value as string);
  //   if (readOnly) {
  //     return <div>{formattedValue}</div>;
  //   }
  return <Input {...props} value={formattedValue} />;
};

export default FieldDateTime;
