import { Checkbox, Tag } from 'antd';
import { CheckboxGroupProps } from 'antd/lib/checkbox/Group';
import React from 'react';
import ScopeLabel from 'src/components/ScopeLabel';
import { Credential_Scope } from 'src/_generated/models';
import { IField, IFieldBase } from '../Form';

export interface IFieldScopes
  extends CheckboxGroupProps,
    Omit<IFieldBase, 'name' | 'children'> {
  type: 'scopes';
  readOnly?: true;
}

const FieldScopes: React.FC<IFieldScopes> = ({
  options = [
    { label: 'Middleware', value: Credential_Scope.Middleware },
    // { label: 'KRS', value: Credential_Scope.Krs },
  ],
  readOnly,
  ...extra
}) => {
  const { value } = extra;
  const scopes = value?.map(({ scope }: any) => scope);
  if (readOnly) {
    return (
      <>
        {scopes?.map((scope, idx) => (
          <ScopeLabel scope={scope} key={idx} />
        ))}
      </>
    );
  }
  return <Checkbox.Group options={options} {...extra} />;
};

export default FieldScopes;
