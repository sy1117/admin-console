import { Checkbox } from 'antd';
import React from 'react';
import { IFieldBase } from '../Form';

interface IFieldCheckbox extends IFieldBase {
  type: 'checkbox';
}

const FieldCheckbox = ({ label, ...extra }) => (
  <Checkbox {...extra}>{label}</Checkbox>
);

export default FieldCheckbox;
