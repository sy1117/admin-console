import { CopyOutlined } from '@ant-design/icons';
import { Alert, Form, Input, Tooltip } from 'antd';
import React, { useRef, useState } from 'react';
import { IFieldBase } from '../Form';

export interface IFieldSecretKey extends IFieldBase {
  type: 'secretkey';
}

const FieldSecretKey: React.FC<IFieldSecretKey> = ({ ...props }) => {
  const ref = useRef(null);
  const [copied, setCopied] = useState(false);
  const clickHandler = () => {
    if (ref?.current) {
      const text = (ref.current as any).input.value;
      setCopied(true);
      navigator.clipboard.writeText(text);
    }
  };

  const tooltipChange = (state) => {
    if (state === true) setCopied(false);
  };

  const CopyButton = (
    <Tooltip
      mouseLeaveDelay={0.5}
      title={copied ? 'Copied!' : 'Copy'}
      onVisibleChange={tooltipChange}
    >
      <CopyOutlined onClick={clickHandler} />
    </Tooltip>
  );

  return (
    <>
      <Input ref={ref} suffix={CopyButton} {...props} />
      <br />
      <br />
      <Alert
        message="Client Secret을 잃어버리면 복구할 수 없습니다. 복사하거나 다운로드 한 후 안전하게 저장해주세요."
        type="warning"
        showIcon
      />
    </>
  );
};

export default FieldSecretKey;
