import { Button } from 'antd';
import { ButtonSize } from 'antd/lib/button';
import React from 'react';
import { IFieldBase } from '../Form';

export interface IFieldSubmit extends Omit<IFieldBase, 'name'> {
  type: 'submit';
  label: string;
  size?: ButtonSize;
}

const FieldSubmit: React.FC<IFieldSubmit> = ({
  type,
  label,
  size = 'middle',
  ...props
}) => {
  return (
    <Button size={size} type="primary" htmlType="submit" {...props}>
      {label}
    </Button>
  );
};

export default FieldSubmit;
