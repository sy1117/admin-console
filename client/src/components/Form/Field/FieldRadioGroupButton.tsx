import { Radio, RadioGroupProps } from 'antd';
import React from 'react';
import { IField, IFieldBase } from '../Form';
export interface IRadioGroupOptions extends IFieldBase {
  value: string;
  text: string;
}
export interface IFieldRadioGroupOptions
  extends RadioGroupProps,
    Omit<IFieldBase, 'children' | 'name'> {
  type: 'radiogroupbutton';
  groupOptions: Array<IRadioGroupOptions>;
}

const FieldRadioGroupButton: React.FC<IFieldRadioGroupOptions> = ({
  groupOptions,
  name,
  ...extra
}) => (
  <Radio.Group name={name} {...extra}>
    {(groupOptions as Array<IRadioGroupOptions>)?.map(({ value, text }) => (
      <Radio.Button value={value}>{text}</Radio.Button>
    ))}
  </Radio.Group>
);

export default FieldRadioGroupButton;
