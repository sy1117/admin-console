import { Input, InputProps } from 'antd';
import React from 'react';
import { IFieldBase } from '../Form';

export interface IFieldText
  extends InputProps,
    Omit<IFieldBase, 'name' | 'children' | 'onReset'> {
  type: 'text';
}

const FieldText: React.FC<IFieldText> = ({ type, ...props }) => {
  return <Input {...props} />;
};

export default FieldText;
