import { Checkbox, CheckboxOptionType } from 'antd';
import {
  AbstractCheckboxGroupProps,
  CheckboxGroupProps,
} from 'antd/lib/checkbox/Group';
import React from 'react';
import { IField, IFieldBase } from '../Form';

export interface IFieldCheckboxGroup
  extends CheckboxGroupProps,
    Omit<IFieldBase, 'name' | 'children'> {
  type: 'checkboxgroup';
  readOnly?: true;
}

const FieldCheckboxGroup: React.FC<IFieldCheckboxGroup> = ({
  options,
  readOnly,
  ...extra
}) => {
  if (readOnly) {
    const { value } = extra;
    return <Checkbox.Group options={options} {...extra} disabled />;
  }
  return <Checkbox.Group options={options} {...extra} />;
};

export default FieldCheckboxGroup;
