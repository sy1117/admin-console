import React, { useState } from 'react';
import { InputProps, Input } from 'antd';
import {
  EyeInvisibleOutlined,
  EyeTwoTone,
  LockOutlined,
} from '@ant-design/icons';
import { IFieldBase } from '../Form';

export interface IFieldPassword
  extends InputProps,
    Omit<IFieldBase, 'name' | 'children' | 'onReset'> {
  type: 'password';
}

const FieldPassword: React.FC<IFieldPassword> = ({ ...props }) => {
  const visible = useState<boolean>(false);

  return (
    <Input.Password
      iconRender={(visible: boolean) => {
        return visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />;
      }}
      {...props}
    />
  );
};

export default FieldPassword;
