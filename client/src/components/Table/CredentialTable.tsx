import { DeleteOutlined } from '@ant-design/icons';
import { Button, Modal, Table, Tag } from 'antd';
import { ColumnType } from 'antd/lib/table';
import React, { useState } from 'react';
import { CredentialClient, Credential_Scope } from 'src/_generated/models';
import _ from 'lodash';
import { convertDatetime } from 'src/lib/datetime';
import ScopeLabel from '../ScopeLabel';

export interface ICredentialTable {
  dataSource?: CredentialClient[] | any;
  onDelete: (clientId: string) => void;
  onRowClick: (record, rowIndex) => void;
}

const CredentialTable: React.FC<ICredentialTable> = ({
  dataSource,
  onDelete,
  onRowClick,
}) => {
  const [visible, setVisible] = useState<boolean>(false);
  const columns: ColumnType<CredentialClient>[] = [
    {
      title: 'Name',
      dataIndex: 'credentialName',
    },
    {
      title: 'Client Id',
      dataIndex: 'clientId',
    },
    {
      title: 'Scope',
      dataIndex: 'scopes',
      key: 'scopes',
      render: (scopes) => (
        <>
          {scopes.map(({ scope }, idx) => {
            return <ScopeLabel scope={scope} key={idx} />;
          })}
        </>
      ),
    },
    {
      title: 'Created Time',
      dataIndex: 'createdAt',
      render: (createdAt) => {
        return <span>{convertDatetime(createdAt)}</span>;
      },
    },
    {
      title: 'Action',
      key: 'action',
      width: 100,
      render: (text: string, record) => {
        return (
          <Button
            type="text"
            role="delete"
            icon={<DeleteOutlined />}
            onClick={deleteConfidentialHandler(record.clientId)}
          />
        );
      },
    },
  ];

  const deleteConfidentialHandler = (clientId) => (event) => {
    event.stopPropagation();
    const modal = Modal.confirm({
      title: `Credential 삭제`,
      content: `Client ID ${clientId} 의 Credential을 삭제하시겠습니까?
    Credential을 삭제하면 더이상 사용할 수 없으며, 복구할 수 없습니다. `,
      okText: '삭제',
      cancelText: '닫기',
      onOk: async (event) => {
        await onDelete(clientId);
        setVisible(false);
        modal.destroy();
        return true;
      },
    });
  };

  return (
    <>
      <Table
        columns={columns}
        dataSource={dataSource}
        rowKey="clientId"
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              if (onRowClick) onRowClick(record, rowIndex);
            }, // click row
          };
        }}
      />
    </>
  );
};

export default CredentialTable;
