import { Tag } from 'antd';
import React from 'react';
import { Credential_Scope } from 'src/_generated/models';

export interface IScopeLabel {
  scope: Credential_Scope;
}

const ScopeLabel: React.FC<IScopeLabel> = ({ scope }) => {
  const ScopeColorMap = {
    KRS: 'geekblue',
    MIDDLEWARE: 'cyan',
  };

  return (
    <Tag color={ScopeColorMap[scope]} key={scope}>
      {scope === Credential_Scope.Middleware && 'Middleware'}
      {scope === Credential_Scope.Krs && 'KRS'}
    </Tag>
  );
};

export default ScopeLabel;
