import React from 'react';
import { Spin } from 'antd';

const style = {
  width: '100%',
  height: '100vh',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  maxHeight: 'none',
};

const Loading: React.FC = ({ children }) => (
  <Spin tip="Loading..." style={style}>
    {children}
  </Spin>
);

export default Loading;
