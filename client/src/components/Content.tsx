import styles from './Content.module.scss';

const Content: React.FC = ({ children }) => (
  <div className={styles.content}>{children}</div>
);

export default Content;
