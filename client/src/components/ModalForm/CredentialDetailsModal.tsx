import { Modal } from 'antd';
import { CredentialClient } from 'src/_generated/models';
import Form, { IFields } from '../Form/Form';

const DETAILS_FIELD: IFields<CredentialClient> = [
  {
    type: 'text',
    label: 'Name',
    name: 'credentialName',
    readOnly: true,
  },
  {
    type: 'text',
    label: 'Client Id',
    name: 'clientId',
    readOnly: true,
  },
  {
    type: 'scopes',
    label: 'Scopes',
    name: 'scopes',
    readOnly: true,
  },
  {
    type: 'datetime',
    label: 'Created Time',
    name: 'createdAt',
    readOnly: true,
  },
];

const CredentialDetailsModal = ({ visible, onCancel, form }) => (
  <Modal
    title="Credential 상세"
    visible={visible}
    okButtonProps={{ hidden: true }}
    cancelText="닫기"
    onCancel={onCancel}
  >
    <Form form={form} fields={DETAILS_FIELD} />
  </Modal>
);

export default CredentialDetailsModal;
