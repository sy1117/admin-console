import { FormProps, Modal, ModalProps } from 'antd';
import React, { useEffect, useState } from 'react';
import {
  CreateCredentialMutation,
  CreateCredentialMutationVariables,
} from 'src/_generated/models';
import Form, { IFields } from '../Form/Form';

const CREATE_CREDENTIAL_FIELDS: IFields<
  CreateCredentialMutationVariables & { credentialName: string }
> = [
  {
    type: 'text',
    label: 'Name',
    name: 'credentialName',
    rules: [{ required: true }],
  },
  {
    type: 'scopes',
    label: 'Scopes',
    name: 'scopes',
    rules: [{ required: true }],
  },
];

const CREATED_CREDENTIAL_FIELD: IFields<
  CreateCredentialMutation['createCredential'] &
    CreateCredentialMutationVariables
> = [
  {
    type: 'text',
    label: 'Name',
    name: 'credentialName',
    readOnly: true,
  },
  {
    type: 'text',
    label: 'Client Id',
    name: 'clientId',
    readOnly: true,
  },
  {
    type: 'scopes',
    label: 'Scopes',
    name: 'scopes',
    readOnly: true,
  },
  {
    type: 'secretkey',
    label: 'Client Secret Key',
    name: 'secret',
    readOnly: true,
  },
];

export enum Step {
  Init,
  Created,
}

const OK_BEFORE_CREATION = '생성';
const OK_AFTER_CREATION = '다운로드';

export interface CreateCredentialsModalProps
  extends Pick<ModalProps, 'visible' | 'onOk' | 'onCancel'>,
    Pick<FormProps, 'form' | 'onFinish'> {}

const CreateCredentialsModal: React.FC<CreateCredentialsModalProps> = ({
  visible,
  onOk,
  onCancel,
  onFinish,
  form,
}) => {
  const [step, setstep] = useState<Step>(Step.Init);
  const [createFormFields, setCreateFormFields] = useState<IFields<any>>(
    CREATE_CREDENTIAL_FIELDS,
  );

  const finishHandler = async (values) => {
    if (onFinish) {
      const result = await onFinish(values);
      if ((result as any)?.data) {
        setstep(Step.Created);
      }
    }
  };
  useEffect(() => {
    return () => {
      form?.resetFields();
      setstep(Step.Init);
    };
  }, []);

  useEffect(() => {
    if (step === Step.Created) {
      setCreateFormFields(CREATED_CREDENTIAL_FIELD);
    }
    return () => {};
  }, [step]);

  return (
    <Modal
      title="Credential 생성"
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
      okText={step === Step.Init ? OK_BEFORE_CREATION : OK_AFTER_CREATION}
      cancelText="닫기"
      maskClosable={false}
    >
      <Form form={form} fields={createFormFields} onFinish={finishHandler} />
    </Modal>
  );
};

export default CreateCredentialsModal;
