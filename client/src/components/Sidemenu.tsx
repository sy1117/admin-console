import { Menu } from 'antd';
import Sider from 'antd/lib/layout/Sider';
import SubMenu from 'antd/lib/menu/SubMenu';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { SideMenuRoutes } from 'src/lib/routes';
import styles from './Sidemenu.module.scss';
import { IconNewWindow } from './Icon/Icon';

const Logo = () => <div className={styles.logo}>ChainZ Admin Console</div>;

const Sidemenu = () => {
  const [current, setCurrent] = useState('0');
  const [openKeys, setOpenKeys] = useState<string>('');
  const history = useHistory();
  const { location } = history;
  const handleMenu = ({ item, key, keyPath, domEvent }: any) => {
    const [menu, submenu]: [string, string] = key.split('-');
    let path = '';
    if (menu) {
      if (SideMenuRoutes[menu].externalLink) {
        return;
      }
      path += SideMenuRoutes[menu].path;

      if (
        submenu &&
        SideMenuRoutes[menu].subPath?.length &&
        SideMenuRoutes[menu].subPath[submenu]
      ) {
        path += SideMenuRoutes[menu].subPath[submenu].path;
      }
    }
    history.push(path);
  };

  const onOpenChange = (keys) => {
    const [_, openMenu] = keys;
    setOpenKeys(openMenu);
  };

  useEffect(() => {
    const { pathname } = location;
    const [_, menuPath, submenuPath] = pathname.split('/');
    const keys: string[] = [];
    if (menuPath) {
      const menuIdx = SideMenuRoutes.findIndex(
        ({ path }) => path === `/${menuPath}`,
      );
      const { subPath } = SideMenuRoutes[menuIdx];
      if (menuIdx > -1) {
        keys.push(`${menuIdx}`);
        if (subPath) {
          const submenuPathIdx = subPath.findIndex(
            ({ path }) => path === `/${submenuPath}`,
          );
          if (submenuPathIdx > -1) {
            keys.push(`${submenuPathIdx}`);
          }
          setOpenKeys(`sub${menuIdx}`);
        }
      }
    }
    setCurrent(keys.join('-'));
  }, [location]);

  return (
    <Sider width={248} theme="light" breakpoint="lg" collapsedWidth="0">
      <Logo />
      {SideMenuRoutes.map(
        ({ title, icon: Icon, subPath, externalLink }, idx) => (
          <Menu
            mode="inline"
            selectedKeys={[current]}
            defaultOpenKeys={[openKeys]}
            openKeys={[openKeys]}
            onOpenChange={onOpenChange}
            onClick={handleMenu}
            key={idx}
          >
            {subPath && (
              <SubMenu title={title} key={`sub${idx}`} icon={<Icon />}>
                {subPath?.map(({ title }, sIdx) => (
                  <Menu.Item key={`${idx}-${sIdx}`}>{title}</Menu.Item>
                ))}
              </SubMenu>
            )}
            {!subPath && (
              <Menu.Item
                key={idx}
                title={title}
                icon={<Icon />}
                style={{
                  height: '40px',
                }}
              >
                {title}
                {externalLink && (
                  <a
                    rel="noreferrer"
                    href={externalLink}
                    style={{ float: 'right' }}
                    target="_blank"
                  >
                    <IconNewWindow />
                  </a>
                )}
              </Menu.Item>
            )}
          </Menu>
        ),
      )}
    </Sider>
  );
};

export default Sidemenu;
