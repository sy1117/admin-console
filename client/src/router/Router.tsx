import { useRef, useEffect } from 'react';
import { useReactiveVar } from '@apollo/client';
import { useWhoAmIQuery } from 'src/_generated/models';
import { useLocation, useHistory, BrowserRouter } from 'react-router-dom';
import { authVar } from '../store/Auth';
import PrivateRouter from './PrivateRouter';
import PublicRouter from './PublicRouter';

const Router = () => {
  const loggedIn = useRef(false);
  const { isLoggedIn } = useReactiveVar(authVar);
  const location = useLocation<{ from?: string }>();
  const history = useHistory();
  const { loading } = useWhoAmIQuery({
    onCompleted(data) {
      authVar({
        isLoggedIn: true,
        user: data.whoAmI,
      });
    },
  });

  useEffect(() => {
    if (loggedIn.current && !loading) {
      const redirect = location.state?.from || '/';
      history.replace(redirect, {});
    }
  }, [loading]);

  useEffect(() => {
    if (!loggedIn.current && isLoggedIn) {
      const redirect = location.state?.from || '/';
      history.replace(redirect, {});
    }
  }, [isLoggedIn]);

  return isLoggedIn ? <PrivateRouter /> : <PublicRouter />;
};

const Wrapper = () => (
  <BrowserRouter>
    <Router />
  </BrowserRouter>
);

export default Wrapper;
