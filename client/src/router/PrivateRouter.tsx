import { Route, Switch, Redirect } from 'react-router-dom';
import { SideMenuRoutes } from '../lib/routes';
import SubRouter from './SubRouter';

const PrivateRouter: React.FC = () => {
  return (
    <Switch>
      {SideMenuRoutes.map(
        ({ path, component, subPath, key, externalLink }, idx) => {
          if (subPath) {
            return (
              <SubRouter
                path={path}
                component={component}
                subPath={subPath}
                key={idx}
              />
            );
          }
          if (externalLink) return;
          return <Route path={path} exact component={component} key={key} />;
        },
      )}
      <Route path="*">
        not found
        <Redirect to="/" />
        {/* <Link to="/">go to main</Link> */}
      </Route>
    </Switch>
  );
};

export default PrivateRouter;
