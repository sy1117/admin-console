import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { IRouteData } from 'src/lib/routes';

interface SubRouterProps {
  path: string;
  component: any;
  subPath: Array<IRouteData>;
}

const SubRouter: React.FC<SubRouterProps> = (
  { path, component, subPath },
  idx,
) => (
  <Switch>
    {subPath?.map(({ path: depth2Path, component: depth2Component }) => (
      <Route
        path={`${path}${depth2Path}`}
        component={depth2Component}
        exact
        key={idx}
      />
    ))}
    <Route path={path} exact component={component} />
  </Switch>
);

export default SubRouter;
