import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
  useLocation,
} from 'react-router-dom';
import { LoggedOutRoutes } from '../lib/routes';

const PublicRouter: React.FC = () => {
  const location = useLocation();

  return (
    <Switch>
      pubic
      {LoggedOutRoutes.map(({ path, component }, idx) => (
        <Route path={path} exact component={component} key={idx} />
      ))}
      <Route path="*">
        <Redirect
          to={{
            pathname: '/signin',
            state: {
              from: location.pathname,
            },
          }}
        />
      </Route>
    </Switch>
  );
};

export default PublicRouter;
