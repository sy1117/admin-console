import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
};

export enum Credential_Provider_Status {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export enum Credential_Scope {
  Middleware = 'MIDDLEWARE',
  Krs = 'KRS'
}

export type CreateCredentialInput = {
  /** Name */
  name: Scalars['String'];
  /** Scope: MIDDLEWARE,KRS */
  scopes: Array<Credential_Scope>;
};

export type CreateUserInput = {
  /** Email */
  email: Scalars['String'];
  /** Password */
  password: Scalars['String'];
  /** Password Check */
  passwordCheck: Scalars['String'];
  /** Name */
  name: Scalars['String'];
};

export type CredentialClient = {
  __typename?: 'CredentialClient';
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  credentialName: Scalars['String'];
  clientId: Scalars['String'];
  secret: Scalars['String'];
  scopes: Array<CredentialClientScope>;
};

export type CredentialClientScope = {
  __typename?: 'CredentialClientScope';
  scope: Credential_Scope;
  credentialClient: CredentialClient;
};

export type CredentialProvider = {
  __typename?: 'CredentialProvider';
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  providerName: Scalars['String'];
  providerCode: Scalars['String'];
  status: Credential_Provider_Status;
  credentialClients: Array<CredentialClient>;
  user: User;
};


export type Mutation = {
  __typename?: 'Mutation';
  createUser: User;
  updateUser: User;
  removeUser: User;
  login: User;
  logout: User;
  createCredential: CredentialClient;
  deleteCredential: Scalars['Boolean'];
};


export type MutationCreateUserArgs = {
  createUserInput: CreateUserInput;
};


export type MutationUpdateUserArgs = {
  updateUserInput: UpdateUserInput;
  email: Scalars['String'];
};


export type MutationRemoveUserArgs = {
  email: Scalars['String'];
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  email: Scalars['String'];
};


export type MutationCreateCredentialArgs = {
  createCredentialInput: CreateCredentialInput;
};


export type MutationDeleteCredentialArgs = {
  clientId: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  healthCheck: Scalars['Boolean'];
  users?: Maybe<Array<User>>;
  user?: Maybe<User>;
  whoAmI: User;
  credentials: Array<CredentialClient>;
  credential: CredentialClient;
};


export type QueryUserArgs = {
  email: Scalars['String'];
};


export type QueryCredentialArgs = {
  clientId: Scalars['String'];
};

export type UpdateUserInput = {
  /** Currnet password */
  currentPassword: Scalars['String'];
  /** New password */
  newPassword?: Maybe<Scalars['String']>;
  /** New password check */
  newPasswordCheck?: Maybe<Scalars['String']>;
  /** Name */
  name?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  createdAt: Scalars['DateTime'];
  updatedAt: Scalars['DateTime'];
  email: Scalars['String'];
  name: Scalars['String'];
  signinFailCount: Scalars['Float'];
  isLocked: Scalars['Boolean'];
  credentialProvider: CredentialProvider;
};

export type CreateCredentialMutationVariables = Exact<{
  name: Scalars['String'];
  scopes: Array<Credential_Scope> | Credential_Scope;
}>;


export type CreateCredentialMutation = (
  { __typename?: 'Mutation' }
  & { createCredential: (
    { __typename?: 'CredentialClient' }
    & Pick<CredentialClient, 'secret' | 'clientId' | 'credentialName' | 'createdAt'>
    & { scopes: Array<(
      { __typename?: 'CredentialClientScope' }
      & Pick<CredentialClientScope, 'scope'>
    )> }
  ) }
);

export type CredentialsQueryVariables = Exact<{ [key: string]: never; }>;


export type CredentialsQuery = (
  { __typename?: 'Query' }
  & { credentials: Array<(
    { __typename?: 'CredentialClient' }
    & Pick<CredentialClient, 'clientId' | 'credentialName' | 'createdAt'>
    & { scopes: Array<(
      { __typename?: 'CredentialClientScope' }
      & Pick<CredentialClientScope, 'scope'>
    )> }
  )> }
);

export type DeleteCredentialMutationVariables = Exact<{
  clientId: Scalars['String'];
}>;


export type DeleteCredentialMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteCredential'>
);

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login: (
    { __typename?: 'User' }
    & Pick<User, 'email' | 'name'>
  ) }
);

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = (
  { __typename?: 'Mutation' }
  & { logout: (
    { __typename?: 'User' }
    & Pick<User, 'email' | 'name'>
  ) }
);

export type SignUpMutationVariables = Exact<{
  email: Scalars['String'];
  name: Scalars['String'];
  password: Scalars['String'];
  passwordCheck: Scalars['String'];
}>;


export type SignUpMutation = (
  { __typename?: 'Mutation' }
  & { createUser: (
    { __typename?: 'User' }
    & Pick<User, 'email' | 'name'>
  ) }
);

export type WhoAmIQueryVariables = Exact<{ [key: string]: never; }>;


export type WhoAmIQuery = (
  { __typename?: 'Query' }
  & { whoAmI: (
    { __typename?: 'User' }
    & Pick<User, 'email' | 'name'>
  ) }
);


export const CreateCredentialDocument = gql`
    mutation createCredential($name: String!, $scopes: [CREDENTIAL_SCOPE!]!) {
  createCredential(createCredentialInput: {name: $name, scopes: $scopes}) {
    secret
    clientId
    credentialName
    createdAt
    scopes {
      scope
    }
  }
}
    `;
export type CreateCredentialMutationFn = Apollo.MutationFunction<CreateCredentialMutation, CreateCredentialMutationVariables>;

/**
 * __useCreateCredentialMutation__
 *
 * To run a mutation, you first call `useCreateCredentialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCredentialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCredentialMutation, { data, loading, error }] = useCreateCredentialMutation({
 *   variables: {
 *      name: // value for 'name'
 *      scopes: // value for 'scopes'
 *   },
 * });
 */
export function useCreateCredentialMutation(baseOptions?: Apollo.MutationHookOptions<CreateCredentialMutation, CreateCredentialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCredentialMutation, CreateCredentialMutationVariables>(CreateCredentialDocument, options);
      }
export type CreateCredentialMutationHookResult = ReturnType<typeof useCreateCredentialMutation>;
export type CreateCredentialMutationResult = Apollo.MutationResult<CreateCredentialMutation>;
export type CreateCredentialMutationOptions = Apollo.BaseMutationOptions<CreateCredentialMutation, CreateCredentialMutationVariables>;
export const CredentialsDocument = gql`
    query credentials {
  credentials {
    scopes {
      scope
    }
    clientId
    credentialName
    createdAt
  }
}
    `;

/**
 * __useCredentialsQuery__
 *
 * To run a query within a React component, call `useCredentialsQuery` and pass it any options that fit your needs.
 * When your component renders, `useCredentialsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCredentialsQuery({
 *   variables: {
 *   },
 * });
 */
export function useCredentialsQuery(baseOptions?: Apollo.QueryHookOptions<CredentialsQuery, CredentialsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CredentialsQuery, CredentialsQueryVariables>(CredentialsDocument, options);
      }
export function useCredentialsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CredentialsQuery, CredentialsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CredentialsQuery, CredentialsQueryVariables>(CredentialsDocument, options);
        }
export type CredentialsQueryHookResult = ReturnType<typeof useCredentialsQuery>;
export type CredentialsLazyQueryHookResult = ReturnType<typeof useCredentialsLazyQuery>;
export type CredentialsQueryResult = Apollo.QueryResult<CredentialsQuery, CredentialsQueryVariables>;
export const DeleteCredentialDocument = gql`
    mutation deleteCredential($clientId: String!) {
  deleteCredential(clientId: $clientId)
}
    `;
export type DeleteCredentialMutationFn = Apollo.MutationFunction<DeleteCredentialMutation, DeleteCredentialMutationVariables>;

/**
 * __useDeleteCredentialMutation__
 *
 * To run a mutation, you first call `useDeleteCredentialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCredentialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCredentialMutation, { data, loading, error }] = useDeleteCredentialMutation({
 *   variables: {
 *      clientId: // value for 'clientId'
 *   },
 * });
 */
export function useDeleteCredentialMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCredentialMutation, DeleteCredentialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCredentialMutation, DeleteCredentialMutationVariables>(DeleteCredentialDocument, options);
      }
export type DeleteCredentialMutationHookResult = ReturnType<typeof useDeleteCredentialMutation>;
export type DeleteCredentialMutationResult = Apollo.MutationResult<DeleteCredentialMutation>;
export type DeleteCredentialMutationOptions = Apollo.BaseMutationOptions<DeleteCredentialMutation, DeleteCredentialMutationVariables>;
export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    email
    name
  }
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LogoutDocument = gql`
    mutation logout {
  logout {
    email
    name
  }
}
    `;
export type LogoutMutationFn = Apollo.MutationFunction<LogoutMutation, LogoutMutationVariables>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: Apollo.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, options);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = Apollo.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = Apollo.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const SignUpDocument = gql`
    mutation signUp($email: String!, $name: String!, $password: String!, $passwordCheck: String!) {
  createUser(
    createUserInput: {email: $email, name: $name, password: $password, passwordCheck: $passwordCheck}
  ) {
    email
    name
  }
}
    `;
export type SignUpMutationFn = Apollo.MutationFunction<SignUpMutation, SignUpMutationVariables>;

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      email: // value for 'email'
 *      name: // value for 'name'
 *      password: // value for 'password'
 *      passwordCheck: // value for 'passwordCheck'
 *   },
 * });
 */
export function useSignUpMutation(baseOptions?: Apollo.MutationHookOptions<SignUpMutation, SignUpMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, options);
      }
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = Apollo.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = Apollo.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>;
export const WhoAmIDocument = gql`
    query whoAmI {
  whoAmI {
    email
    name
  }
}
    `;

/**
 * __useWhoAmIQuery__
 *
 * To run a query within a React component, call `useWhoAmIQuery` and pass it any options that fit your needs.
 * When your component renders, `useWhoAmIQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useWhoAmIQuery({
 *   variables: {
 *   },
 * });
 */
export function useWhoAmIQuery(baseOptions?: Apollo.QueryHookOptions<WhoAmIQuery, WhoAmIQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<WhoAmIQuery, WhoAmIQueryVariables>(WhoAmIDocument, options);
      }
export function useWhoAmILazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<WhoAmIQuery, WhoAmIQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<WhoAmIQuery, WhoAmIQueryVariables>(WhoAmIDocument, options);
        }
export type WhoAmIQueryHookResult = ReturnType<typeof useWhoAmIQuery>;
export type WhoAmILazyQueryHookResult = ReturnType<typeof useWhoAmILazyQuery>;
export type WhoAmIQueryResult = Apollo.QueryResult<WhoAmIQuery, WhoAmIQueryVariables>;