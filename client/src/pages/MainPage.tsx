import { Button, Modal } from 'antd';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import Content from 'src/components/Content';
import BasicLayout from 'src/layouts/BasicLayout';

const MainPage = () => {
  const [isOpen, setOpen] = useState<boolean>(false);
  const showModal = () => {
    setOpen(true);
  };

  const handleOk = () => {
    setOpen(false);
  };
  const handleCancel = () => {
    setOpen(false);
  };

  return <Redirect to="/security/credential" />;

  // return (
  //   <BasicLayout>
  //     <Content>
  //       main
  //       <Button onClick={showModal}>show Modal</Button>
  //       <Modal
  //         title="Basic Modal"
  //         visible={isOpen}
  //         onOk={handleOk}
  //         onCancel={handleCancel}
  //       />
  //     </Content>
  //   </BasicLayout>
  // );
};

export default MainPage;
