import { ApolloError, useReactiveVar } from '@apollo/client';
import {
  LoginMutationVariables,
  useLoginMutation,
} from 'src/_generated/models';
import React from 'react';
import { Typography } from 'antd';
import { authVar } from '../store/Auth';
import LoginLayout from '../layouts/EmptyLayout';
import styles from './LoginPage.module.scss';
import Form, { IFields } from '../components/Form/Form';

const { Title } = Typography;

const LOGIN_FIELDS: IFields<LoginMutationVariables> = [
  {
    size: 'large',
    type: 'text',
    name: 'email',
    placeholder: '이메일 주소',
    rules: [{ required: true }],
  },
  {
    type: 'password',
    name: 'password',
    placeholder: '비밀번호',
    rules: [{ required: true }],
    size: 'large',
  },
  {
    type: 'submit',
    label: '로그인',
    size: 'large',
  },
];

const LoginPage: React.FC = () => {
  const [login] = useLoginMutation({
    onError(error: ApolloError) {},
    onCompleted({ login: { email, name } }) {
      authVar({
        isLoggedIn: true,
        user: {
          email,
          name,
        },
      });
    },
  });

  const onFinish = (values: any) => {
    const { email, password } = values;
    try {
      login({
        variables: {
          email,
          password,
        },
      });
    } catch (error) {
      // console.error(error);
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <LoginLayout>
      <Title level={3} className={styles.logo}>
        ChainZ Admin Console
      </Title>
      <h1 className={styles.title}>로그인</h1>
      <Form
        fields={LOGIN_FIELDS}
        onFinish={onFinish}
        initialValues={{ remember: true }}
      >
        <div className={styles.links}>
          {/* <a className={styles.link} href="/find-id" >
            아이디 찾기
          </a> */}
          <span className={styles.link}>아이디 찾기</span>
          <span className={styles.divider}>|</span>
          {/* <a className={styles.link} href="/reset-password">
            비밀번호 찾기
          </a> */}
          <span className={styles.link}>비밀번호 찾기</span>
          <span className={styles.divider}>|</span>
          <a className={styles.link} href="/signup">
            회원가입
          </a>
        </div>
      </Form>
    </LoginLayout>
  );
};

export default LoginPage;
