import EmptyLayout from 'src/layouts/EmptyLayout';
import { useForm } from 'antd/lib/form/Form';
import { Button, Modal } from 'antd';
import {
  useSignUpMutation,
  SignUpMutationVariables,
} from 'src/_generated/models';
import { useHistory } from 'react-router-dom';
import { SizeType } from 'antd/lib/config-provider/SizeContext';
import Form, { IFields } from '../components/Form/Form';

const COMMON_FIELD_PROPS = {
  size: 'large' as SizeType,
  rules: [{ required: true }],
};

const SIGNUP_FIELDS: IFields<SignUpMutationVariables> = [
  {
    ...COMMON_FIELD_PROPS,
    type: 'text',
    name: 'email',
    placeholder: '이메일',
  },
  {
    ...COMMON_FIELD_PROPS,
    type: 'text',
    name: 'name',
    placeholder: '이름',
    rules: [{ required: true }],
  },
  {
    ...COMMON_FIELD_PROPS,
    type: 'password',
    name: 'password',
    placeholder: '패스워드',
  },
  {
    ...COMMON_FIELD_PROPS,
    type: 'password',
    name: 'passwordCheck',
    placeholder: '패스워드 확인',
  },
];

const SignUpPage: React.FC = () => {
  const history = useHistory();
  const [form] = useForm<any>();
  const [signUp] = useSignUpMutation();
  const finishHandler = async ({
    email,
    name,
    password,
    passwordCheck,
  }: SignUpMutationVariables) => {
    const { data } = await signUp({
      variables: {
        email,
        name,
        password,
        passwordCheck,
      },
    });
    if (data) {
      const modal = Modal.confirm({
        title: '회원 가입이 완료되었습니다.',
        content: '로그인 후 이용해주세요.',
        okText: '확인',
        cancelButtonProps: { hidden: true },
        onOk: async (event) => {
          history.replace('/signin');
        },
      });
    }
  };
  return (
    <EmptyLayout>
      <h1>회원가입</h1>
      <Form form={form as any} fields={SIGNUP_FIELDS} onFinish={finishHandler}>
        <Button
          size="large"
          onClick={() => {
            form.submit();
          }}
        >
          회원가입
        </Button>
      </Form>
    </EmptyLayout>
  );
};

export default SignUpPage;
