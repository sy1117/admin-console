import { Button, Form, Modal, Space, Table, Tag } from 'antd';
import React, { useState } from 'react';
import Content from 'src/components/Content';
import IPWhitelistForm from 'src/components/Form/IPWhitelistForm';
import PageHeaderLayout from 'src/layouts/PageHeaderLayout';

const IPWhitelistPage = () => {
  const [visible, setVisible] = useState(false);

  const showUserModal = () => {
    setVisible(true);
  };

  const hideUserModal = () => {
    setVisible(false);
  };

  const onFinish = (values: any) => {
    console.log('Finish:', values);
  };

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      render: (text: string) => <a>{text}</a>,
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Tags',
      key: 'tags',
      dataIndex: 'tags',
      render: (tags: Array<any>) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 5 ? 'geekblue' : 'green';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (text: string, record: { name: string }) => (
        <Space size="middle">
          <a href="">
            Invite
            <span>{record.name}</span>
          </a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

  const data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
      tags: ['loser'],
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
      tags: ['cool', 'teacher'],
    },
  ];

  return (
    <PageHeaderLayout>
      <Content>
        <Button
          type="primary"
          style={{ float: 'right' }}
          onClick={showUserModal}
        >
          Primary Button
        </Button>
        <br />
        <br />
        <Table dataSource={data} columns={columns} />
        <Form.Provider
          onFormFinish={(name, { values, forms }) => {
            if (name === 'whitelistForm') {
              const { whitelistForm } = forms;
              console.log(whitelistForm.getFieldsValue());
              // const { basicForm } = forms;
              // const users = basicForm.getFieldValue('users') || [];
              // basicForm.setFieldsValue({ users: [...users, values] });
              // setVisible(false);
            }
          }}
        >
          <IPWhitelistForm visible={visible} onCancel={hideUserModal} />
        </Form.Provider>
      </Content>
    </PageHeaderLayout>
  );
};

export default IPWhitelistPage;
