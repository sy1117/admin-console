import { Button, message, Modal } from 'antd';
import { useState } from 'react';
import Content from 'src/components/Content';
import { useForm } from 'src/components/Form/Form';
import CredentialTable from 'src/components/Table/CredentialTable';
import PageHeaderLayout from 'src/layouts/PageHeaderLayout';
import saveAsFile from 'src/lib/saveAsFile';
import _ from 'lodash';
import {
  useCredentialsQuery,
  useCreateCredentialMutation,
  useDeleteCredentialMutation,
  CredentialsDocument,
} from 'src/_generated/models';
import { ApolloError } from '@apollo/client';
import CreateCredentialsModal from 'src/components/ModalForm/CreateCredentialsModal';
import CredentialDetailsModal from 'src/components/ModalForm/CredentialDetailsModal';
import confirmMessages from '../lib/messages/confirmMessages';

const CreateCredentialButton = ({ onClick }) => (
  <Button
    type="primary"
    style={{ float: 'right', marginBottom: '24px' }}
    onClick={onClick}
  >
    + Credential 생성
  </Button>
);
// reset form fields when modal is form, closed

const CredentialPage = () => {
  /**
   * Query Credentials
   */
  const { loading, data, networkStatus } = useCredentialsQuery();
  /**
   * Create Credential
   */
  const [createForm] = useForm();
  const [createModal, setCreateModal] = useState(false);
  const showCreateModal = () => {
    setCreateModal(true);
  };
  const hideCreateModal = () => {
    setCreateModal(false);
  };

  const [createCredentials] = useCreateCredentialMutation({
    refetchQueries: [{ query: CredentialsDocument }],
    onCompleted(data) {
      const { createCredential } = data;
      const { secret, scopes, clientId } = createCredential;
      message.success(
        confirmMessages({
          clientId,
        }).credentialCreated,
      );
      const newData = {
        ...createForm.getFieldsValue(),
        scopes,
        secret,
        clientId,
      };
      createForm.setFieldsValue(newData);
    },
    onError(error: ApolloError) {
      const { message } = error;
      Modal.error({
        title: message,
      });
    },
  });
  /**
   * Detail Credential
   */
  const [detailModal, setDetailModal] = useState(false);
  const showDetailModal = (record) => {
    detailForm.setFieldsValue(record);
    setDetailModal(true);
  };
  const hideDetailModal = () => {
    setDetailModal(false);
  };

  const [detailForm] = useForm();
  /**
   * Delete Credential
   */
  const [deleteCredential] = useDeleteCredentialMutation({
    refetchQueries: [{ query: CredentialsDocument }],
  });
  const deleteCredentialHandler = async (clientId) => {
    try {
      const result = await deleteCredential({
        variables: {
          clientId,
        },
      });
      message.success(confirmMessages({ clientId }).credentialDeleted);
    } catch (error) {
      message.error(confirmMessages({ clientId }).credentialDeletedFailed);
    }
  };

  const createHandler = () => {
    const isSubmitted = createForm.getFieldValue('secret');
    if (isSubmitted === undefined) {
      createForm.submit();
      return false;
    }
    const { clientId, secret, scopes } = createForm.getFieldsValue();

    saveAsFile(
      {
        clientId,
        secret,
        scopes,
      },
      `chainz-credential-${clientId}`,
    );
  };

  const createSubmitHandler = async (data) => {
    const { credentialName, scopes } = data;
    const result = await createCredentials({
      variables: {
        name: credentialName,
        scopes,
      },
    });
    return result;
  };

  const showCredentialModal = (record, rowIndex) => {
    showDetailModal(record);
  };

  return (
    <PageHeaderLayout>
      <Content>
        <CreateCredentialButton onClick={showCreateModal} />
        <CredentialTable
          dataSource={data?.credentials}
          onDelete={deleteCredentialHandler}
          onRowClick={showCredentialModal}
        />
      </Content>
      {createModal && (
        <CreateCredentialsModal
          visible={createModal}
          onOk={createHandler}
          onCancel={hideCreateModal}
          form={createForm}
          onFinish={createSubmitHandler}
        />
      )}
      {detailModal && (
        <CredentialDetailsModal
          visible={detailModal}
          onCancel={hideDetailModal}
          form={detailForm}
        />
      )}
    </PageHeaderLayout>
  );
};

export default CredentialPage;
