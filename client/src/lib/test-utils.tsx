import { BrowserRouter } from 'react-router-dom';
import { render, RenderResult } from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { MockedProvider } from '@apollo/client/testing';
import { ConfigProvider } from 'antd';
import CredentialPage from 'src/pages/CredentialPage';
import client from './apollo';
import validateMessages from '../lib/messages/validationMessages';

export const renderWithRouter = (
  ui,
  { route = '/', mocks = [] }: { route: string; mocks?: Array<any> },
): RenderResult => {
  window.history.pushState({}, 'Test page', route);

  const wrapper: React.FC = ({ children }) => (
    <BrowserRouter>
      <ApolloProvider client={client}>
        <ConfigProvider form={{ validateMessages }}>
          <MockedProvider mocks={mocks} addTypename={false}>
            {children}
          </MockedProvider>
        </ConfigProvider>
      </ApolloProvider>
    </BrowserRouter>
  );

  return render(ui, { wrapper });
};

const index = {
  renderWithRouter,
};

export default index;
