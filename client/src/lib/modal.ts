import { Modal as AntModal, ModalFuncProps } from 'antd';
import alertMessages, { TAlert } from './messages/alertMessages';

type TModal = 'error' | 'info' | 'warning';
const Modal = (type: TModal, title: TAlert, options: ModalFuncProps) => {
  const modalFn = AntModal[type];
  modalFn({
    title: alertMessages[title],
    ...options,
  });
};

export default Modal;
