import _ from 'lodash';

export const appendList = <T extends {}>(list: Array<T>, item: T): Array<T> => {
  const temp = [...list];
  temp.push(item);
  return temp;
};

export const removeList = <T extends {}>(
  list: Array<T>,
  filter: (data: T) => void,
): Array<T> => {
  const temp = [...list];
  _.remove(temp, filter);
  return temp;
};

const List = {
  appendList,
  removeList,
};
export default List;
