import { FunctionComponent } from 'react';
import {
  HomeOutlined,
  BlockOutlined,
  SecurityScanOutlined,
} from '@ant-design/icons';
import SignUpPage from 'src/pages/SignUpPage';
import { Redirect } from 'react-router-dom';
import MainPage from '../pages/MainPage';
import EmptyLayout from '../layouts/EmptyLayout';
import LoginPage from '../pages/LoginPage';
import CredentialPage from '../pages/CredentialPage';
// import IPWhitelistPage from '../pages/IPWhitelistPage';

export interface IRouteData {
  path: string;
  component: FunctionComponent;
  key: string;
}

export interface ISideMenuRouteData extends IRouteData {
  title: string;
  icon?: any;
  externalLink?: string;
  subPath?: ISideMenuRouteData[];
}

export const LoggedOutRoutes: IRouteData[] = [
  {
    path: '/reset-password',
    component: () => {
      return <EmptyLayout>reset-password</EmptyLayout>;
    },
    key: 'reset-password',
  },
  {
    path: '/signin',
    component: LoginPage,
    key: 'signin',
  },
  {
    path: '/signup',
    component: SignUpPage,
    key: 'signup',
  },
  {
    path: '/find-id',
    component: () => {
      return <EmptyLayout>find-id</EmptyLayout>;
    },
    key: 'find-id',
  },
];

export const SideMenuRoutes: ISideMenuRouteData[] = [
  {
    title: 'Home',
    path: '/',
    icon: HomeOutlined,
    component: MainPage,
    key: 'home',
  },
  {
    title: 'Block Explorer',
    path: '/be',
    externalLink: 'http://dev-alethio.chainz.biz/',
    icon: BlockOutlined,
    component: MainPage,
    key: 'block-explore',
  },
  // {
  //   title: 'Middleware',
  //   path: '/mw',
  //   // externalLink: 'http://explorer.chainz.biz/m',
  //   icon: ClusterOutlined,
  //   component: MainPage,
  //   key: 'middleware',
  // },
  {
    title: 'Security',
    path: '/security',
    icon: SecurityScanOutlined,
    component: () => <Redirect to="/security/credential" />,
    key: 'security',
    subPath: [
      {
        title: 'Credential',
        path: '/credential',
        component: CredentialPage,
        key: 'security-credential',
      },
      // {
      //   title: 'IP Whitelist',
      //   path: '/whitelist',
      //   component: IPWhitelistPage,
      //   key: 'security-whitelist',
      // },
    ],
  },
];

export const Routes: IRouteData[] = [];
