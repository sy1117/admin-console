import jsonFormat from 'json-format';

export const saveAsFile = (
  data: object,
  fileName: string,
  fileType = 'application/json',
) => {
  const config = {
    type: 'space',
    size: 2,
  };
  // Create a blob of the data
  const fileToSave = new Blob([jsonFormat(data, config)], {
    type: fileType,
  });

  const a = document.createElement('a');
  a.href = URL.createObjectURL(fileToSave);
  a.download = `${fileName}.json`;
  a.click();
};

export default saveAsFile;
