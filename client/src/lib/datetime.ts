import moment from 'moment';

export const convertDatetime = (datetime: string): string => {
  const formattedCreatedAt = moment
    .utc(`${datetime}`)
    .local()
    .format('YYYY-MM-DD HH:mm:ss');
  return formattedCreatedAt;
};

const index = {
  convertDatetime,
};

export default index;
