import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { Modal } from 'antd';
import { authVar, logout } from 'src/store/Auth';
import alertMessages from './messages/alertMessages';

const Query = {
  fields: {
    isLoggedIn: {
      read() {
        return authVar().isLoggedIn;
      },
    },
    currentUser: {
      read() {
        return authVar().user;
      },
    },
  },
};

// [todo] 환경변수로 변경
const httpLink = new HttpLink({
  uri: '/graphql',
  credentials: 'include',
});

const errorLink = onError(
  ({ graphQLErrors, networkError, response, operation, forward }) => {
    const errors = response?.errors;
    if (graphQLErrors) {
      if (errors) {
        switch (operation.operationName) {
          case 'whoAmI':
            logout();
            break;
          case 'logout':
            // @ts-ignore
            response.errors = null;
            break;
          case 'login': {
            const { message } = errors[0];
            Modal.error({ title: alertMessages.loginFailed, content: message });
            logout();
            break;
          }
          default:
            // @ts-ignore
            response.errors = null;
            return forward(operation);
        }
      }
    } else if (networkError) {
      Modal.error({
        title: alertMessages.networkError,
        content: networkError.message,
      });
    }
  },
);

export const client = new ApolloClient({
  link: errorLink.concat(httpLink),
  credentials: 'include',
  cache: new InMemoryCache({
    addTypename: false,
    typePolicies: {
      Query,
    },
  }),
});

export default client;
