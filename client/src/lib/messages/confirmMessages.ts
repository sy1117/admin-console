export type TConfirm =
  | 'credentialCreated'
  | 'credentialDeleted'
  | 'credentialDeletedFailed';
export type IConfirmMessages = {
  [K in TConfirm]: string;
};

export const confirmMessages = (props?: any): IConfirmMessages => {
  return {
    credentialCreated: 'Credentials 생성이 완료되었습니다.',
    credentialDeleted: `Client Id(${props.clientId})가 삭제되었습니다.`,
    credentialDeletedFailed: `Credential 삭제 중 오류가 발생했습니다. 다시 시도해주세요.`,
  };
};

export default confirmMessages;
