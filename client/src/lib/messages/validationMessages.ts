/* eslint-disable no-template-curly-in-string */
const typeTemplate = "${name}' 은/는 ${type} 형식에 맞지 않습니다";

export const validateMessages: any = {
  default: "Validation error on field '${name}'",
  required: "'${label}' 을/를 입력해주세요",
  enum: "'${label}' must be one of [${enum}]",
  whitespace: "'${label}'는 빈 값이어야 합니다 ",
  date: {
    format: "'${value}' 은 유효한 날짜가 아닙니다",
    parse: "'${value}' could not be parsed as date",
    invalid: "'${value}' is invalid date",
  },
  types: {
    string: typeTemplate,
    method: typeTemplate,
    array: typeTemplate,
    object: typeTemplate,
    number: typeTemplate,
    date: typeTemplate,
    boolean: typeTemplate,
    integer: typeTemplate,
    float: typeTemplate,
    regexp: typeTemplate,
    email: typeTemplate,
    url: typeTemplate,
    hex: typeTemplate,
  },
  string: {
    len: "'${label}' must be exactly ${len} characters",
    min: "'${label}' must be at least ${min} characters",
    max: "'${label}' cannot be longer than ${max} characters",
    range: "'${label}' must be between ${min} and ${max} characters",
  },
  number: {
    len: "'${label}' must equal ${len}",
    min: "'${label}' cannot be less than ${min}",
    max: "'${label}' cannot be greater than ${max}",
    range: "'${label}' must be between ${min} and ${max}",
  },
  array: {
    len: "'${label}' must be exactly ${len} in length",
    min: "'${label}' cannot be less than ${min} in length",
    max: "'${label}' cannot be greater than ${max} in length",
    range: "'${label}' must be between ${min} and ${max} in length",
  },
  pattern: {
    mismatch: "'${label}' does not match pattern ${pattern}",
  },
};

export default validateMessages;
