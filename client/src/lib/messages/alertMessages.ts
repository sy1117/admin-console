export type TAlert = 'loginFailed' | 'networkError';
export type IAlertMessages = {
  [K in TAlert]: string;
};

export const alertMessages: IAlertMessages = {
  loginFailed: '로그인에 실패하였습니다.',
  networkError: '네트워크 오류가 발생하였습니다.',
};

export default alertMessages;
