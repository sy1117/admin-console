import { BreadcrumbProps, PageHeader } from 'antd';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { SideMenuRoutes } from 'src/lib/routes';
import BasicLayout from './BasicLayout';
import styles from './layout.module.scss';

const useBreadCrumb = () => {
  const location = useLocation();
  const splitted = location.pathname.split('/');

  const routes: BreadcrumbProps['routes'] = [];

  routes.push({
    path: '/',
    breadcrumbName: 'Home',
  });

  let currentMenu = SideMenuRoutes.filter(
    ({ path }) => path === `/${splitted[1]}`,
  )[0];

  let currentPath = '';
  if (currentMenu) {
    currentPath = currentMenu.path;
    routes.push({
      path: currentMenu.path,
      breadcrumbName: currentMenu.title,
      // key: currentMenu.key,
    });
    if (currentMenu.subPath) {
      currentMenu = currentMenu?.subPath.filter(
        ({ path }) => path === `/${splitted[2]}`,
      )[0];
      if (currentMenu) {
        routes.push({
          path: [currentMenu.path].join(''),
          breadcrumbName: currentMenu.title,
          // key: currentMenu.key,
        });
      }
    }
  }

  // eslint-disable-next-line no-shadow
  const itemRender = (route: any, params: any, routes: any, paths: any) => {
    const last = routes.indexOf(route) === routes.length - 1;
    return <Link to={['', ...paths].join('/')}>{route.breadcrumbName}</Link>;
  };

  return { itemRender, routes };
};

const PageHeaderLayout: React.FC = ({ children }) => {
  const breadcrumb = useBreadCrumb();

  return (
    <BasicLayout>
      <div className={styles.page_header}>
        <PageHeader
          className={styles.page_header}
          title={breadcrumb.routes[2]?.breadcrumbName}
          breadcrumb={breadcrumb}
        />
      </div>
      {children}
    </BasicLayout>
  );
};

export default PageHeaderLayout;
