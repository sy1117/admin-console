import { useReactiveVar } from '@apollo/client';
import { UserOutlined, LogoutOutlined } from '@ant-design/icons';
import { Avatar, Layout, Menu, Dropdown, Modal } from 'antd';
import { authVar, IAuth } from 'src/store/Auth';
import type { DropDownProps } from 'antd/es/dropdown';
import Sidemenu from 'src/components/Sidemenu';
import { useLogoutMutation } from 'src/_generated/models';
import styles from './layout.module.scss';

const { Header, Content } = Layout;

export type HeaderDropdownProps = {
  overlayClassName?: string;
  overlay: React.ReactNode | (() => React.ReactNode) | any;
  placement?:
    | 'bottomLeft'
    | 'bottomRight'
    | 'topLeft'
    | 'topCenter'
    | 'topRight'
    | 'bottomCenter';
} & Omit<DropDownProps, 'overlay'>;

const HeaderDropdown: React.FC<HeaderDropdownProps> = ({
  overlayClassName: cls,
  ...restProps
}) => <Dropdown {...restProps} />;

const BasicLayout: React.FC = ({ children }) => {
  const { user } = useReactiveVar<IAuth>(authVar);
  const [mutationLogout] = useLogoutMutation({
    onCompleted() {
      Modal.info({
        title: '로그아웃되었습니다',
      });
      window.location.reload();
    },
  });

  const clickMenuHandler = (props: any) => {
    switch (props.key) {
      case 'logout':
        mutationLogout();
        break;
      default:
        break;
    }
  };

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={clickMenuHandler}>
      <Menu.Item key="logout">
        <LogoutOutlined />
        로그아웃
      </Menu.Item>
    </Menu>
  );

  return (
    <Layout className={`${styles.root} ${styles.basic}`} hasSider>
      <Sidemenu />
      <Layout>
        <Header className={styles.header}>
          <span className={styles.middle} />
          <HeaderDropdown
            overlay={menuHeaderDropdown}
            className={styles.profile}
          >
            <div className={styles.profile}>
              <span className={styles.userName}>{user?.name}</span>
              <span className={styles.userImage}>
                <Avatar size="large" icon={<UserOutlined />} />
              </span>
            </div>
          </HeaderDropdown>
        </Header>
        <Content>{children}</Content>
      </Layout>
    </Layout>
  );
};

export default BasicLayout;
