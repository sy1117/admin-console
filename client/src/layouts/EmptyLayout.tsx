import React from 'react';
import styles from './layout.module.scss';

const EmptyLayout: React.FC = ({ children }) => (
  <div className={`${styles.root} ${styles.empty}`}>
    <div className={styles.container}>{children}</div>
  </div>
);

export default EmptyLayout;
